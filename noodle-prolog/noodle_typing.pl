:- module(noodle_typing, [
        ndl_typing/1,
        ndl_typing_init/1,
        ndl_typing_remember_predicate/2,
        ndl_typing_variables/2,
        ndl_typing_of/2,
        ndl_typing_of/3,
        ndl_typing_add/4,
        ndl_typing_add_variable/4,
        ndl_typing_add_predicate/4,
        ndl_typing_promote/3,
        ndl_typing_variable_subtype/2,
        ndl_typing_variable_name/3
    ]).

:- dynamic(ndl_namespace/1).
:- dynamic(arg_type/3).

ndl_typing_init(Namespace) :-
    retractall(ndl_namespace(_)),
    retractall(arg_type(_,_,_)),
    assert(ndl_namespace(Namespace)).

ndl_typing([]).
    
ndl_typing_remember_predicate(Name, Types) :-
    ndl_typing_remember_predicate(Name, Types, 1).
ndl_typing_remember_predicate(_, [], _).
ndl_typing_remember_predicate(Name, [Type|Rest], Index) :-
    assert(arg_type(Name, Index, Type)),
    NIndex is Index + 1,
    ndl_typing_remember_predicate(Name, Rest, NIndex).

ndl_typing_variables([], []).
ndl_typing_variables([Var:_|T], [Var|NT]) :-
    ndl_typing_variables(T, NT).

ndl_typing_of(Var, [Var2:Type|_], Type) :-
    Var == Var2, !.
ndl_typing_of(Var, [_|Rest], Type) :-
    ndl_typing_of(Var, Rest, Type).

ndl_typing_of(Context, Type) :-
    ndl_namespace(N),
    N:type_of(Context, Type_),
    ndl_typing_variable_subtype(Type_, Type).

ndl_typing_add(Var, Type, OldContext, [Var:Type | OldContext]) :- 
    \+ ndl_typing_of(Var, OldContext, Type), !.
ndl_typing_add(_, _, C, C).

ndl_typing_add_variable(Name, Args, OC, NC) :-
    ndl_typing_add_variable(Name, Args, 1, OC, NC).

ndl_typing_add_variable(Name, [Var], _, OC, NC) :-
    !,
    ndl_typing_variable_subtype(var(Name), Type),
    ndl_typing_add(Var, Type, OC, NC).
ndl_typing_add_variable(Name, [Var | Rest], Index, OC, NC) :-
    ndl_namespace(N),
    N:type_of(index(Name, Index), Type),
    ndl_typing_add(Var, Type, OC, TC),
    NIndex is Index + 1,
    ndl_typing_add_variable(Name, Rest, NIndex, TC, NC).

ndl_typing_add_predicate(Name, Variables, OC, NC) :-
    ndl_typing_add_predicate(Name, Variables, 1, OC, NC).
ndl_typing_add_predicate(_, [], _, OC, OC).
ndl_typing_add_predicate(Name, [Var|Rest], Index, OC, NC) :-
    arg_type(Name, Index, Type),
    ndl_typing_add(Var, Type, OC, TC),
    NIndex is Index + 1,
    ndl_typing_add_predicate(Name, Rest, NIndex, TC, NC).

ndl_typing_promote([X], Context, Type) :-
    ndl_typing_of(X, Context, Type),
    !.
ndl_typing_promote([X, Y], Context, Type) :-
    ndl_typing_of(X, Context, Type),
    ndl_typing_of(Y, Context, Type),
    !.
ndl_typing_promote([X, Y], Context, XType) :-
    ndl_typing_of(X, Context, XType),
    ndl_typing_of(Y, Context, YType),
    ndl_typing_is_range(XType),
    \+ ndl_typing_is_range(YType), !.
ndl_typing_promote([X, Y], Context, YType) :-
    ndl_typing_of(X, Context, XType),
    ndl_typing_of(Y, Context, YType),
    ndl_typing_is_range(YType),
    \+ ndl_typing_is_range(XType), !.
ndl_typing_promote(_, _, _) :-
    writef('Compiling error - cannot promote type\n'),
    fail.

ndl_typing_variable_subtype(Type, Type) :-
    Type \= var(_), !.
ndl_typing_variable_subtype(var(Name), fvar(Name)) :-
    ndl_namespace(N),
    current_predicate(N:fixed/1), 
    N:fixed(Name), !.
ndl_typing_variable_subtype(var(Name), rvar(Name)) :-
    ndl_namespace(N),
    current_predicate(N:reified/1),  
    N:reified(Name), !.
ndl_typing_variable_subtype(var(Name), avar(Name)) :-
    ndl_namespace(N),
    current_predicate(N:auxillary/2),  
    N:auxillary(Name, _), !.
ndl_typing_variable_subtype(var(Name), var(Name)).

ndl_typing_variable_name(var(Name), var, Name).
ndl_typing_variable_name(avar(Name), avar, Name).
ndl_typing_variable_name(fvar(Name), fvar, Name).
ndl_typing_variable_name(rvar(Name), rvar, Name).

ndl_typing_range_name(range(Name), Name).
ndl_typing_is_range(X) :- ndl_typing_range_name(X, _).