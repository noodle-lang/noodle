:- module(noodle_model, [
    op(400, yfx, '..'),
    op(400, yfx, 'used_in'),
    ndl_model/1,
    ndl_model/2
]).

:- use_module(noodle).

:- dynamic(ndl_namespace/1).
:- dynamic(ndl_debug/1).
:- dynamic(ndl_constraint_pairs/2).
:- dynamic(ndl_variable_definition/3).
:- dynamic(ndl_type_use/2).
:- dynamic(ndl_range_element/2).
:- dynamic(ndl_serialize_target/1).

% Just to eliminate compiler warnings
% All those predicates are later redeclared
% in a new namespace
% Begin
:- dynamic(constant_definition/2).
:- dynamic(range_definition/2).
:- dynamic(body_predicate/1).
:- dynamic(available_integer/1).
:- dynamic(available_range/1).
:- dynamic(available_variable/1).
:- dynamic(available_constraint/1).
:- dynamic(type_uses/2).
:- dynamic(type_of/2).
:- dynamic(constant/2).
:- dynamic(range_element/2).
:- dynamic(variable/2).
:- dynamic(variable/3).
:- dynamic(variable/4).
:- dynamic(variable/5).
:- dynamic(variable/6).
:- dynamic(variable/7).
:- dynamic(variable/8).
:- dynamic(fixed/1).
:- dynamic(auxillary/2).
:- dynamic(auxillary_semantics/2).
:- dynamic(depends/3).
:- dynamic(constraint/3).
:- dynamic(constraint_semantics/2).
:- dynamic(reified/1).
:- dynamic(reified_constraint/3).
:- dynamic(new_value/2).
:- dynamic(random_variable/2).
:- dynamic(random_variable/3).
:- dynamic(random_variable/4).
:- dynamic(random_variable/5).
:- dynamic(random_variable/6).
:- dynamic(random_variable/7).
:- dynamic(random_variable/8).
:- dynamic(random_range_element/2).
:- dynamic(random_constraint/3).
% End

header_predicate(type_uses/2).
header_predicate(type_of/2).
header_predicate(available_integer/1).
header_predicate(available_range/1).
header_predicate(available_variable/1).
header_predicate(available_constraint/1).
header_predicate(body_predicate/1).
header_predicate(constant_definition/2).
header_predicate(range_definition/2).
header_predicate(reified_constraint/3).

internal_body_predicate(X) :- 
    dynamic_predicate(X, public),
    \+ header_predicate(X).
dynamic_predicate(type_uses/2, public).
dynamic_predicate(type_of/2, public).
dynamic_predicate(available_integer/1, public).
dynamic_predicate(available_range/1, public).
dynamic_predicate(available_variable/1, public).
dynamic_predicate(available_constraint/1, public).
dynamic_predicate(body_predicate/1, public).
dynamic_predicate(constant_definition/2, public).
dynamic_predicate(range_definition/2, public).
dynamic_predicate(constant/2, public).
dynamic_predicate(range_element/2, public).
dynamic_predicate(variable/2, public).
dynamic_predicate(variable/3, public).
dynamic_predicate(variable/4, public).
dynamic_predicate(variable/5, public).
dynamic_predicate(variable/6, public).
dynamic_predicate(variable/7, public).
dynamic_predicate(variable/8, public).
dynamic_predicate(fixed/1, public).
dynamic_predicate(auxillary/2, public).
dynamic_predicate(auxillary_semantics/2, public).
dynamic_predicate(depends/3, public).
dynamic_predicate(constraint/3, public).
dynamic_predicate(constraint_semantics/2, public).
dynamic_predicate(reified/1, public).
dynamic_predicate(reified_constraint/3, public).
dynamic_predicate(random_variable/2, public).
dynamic_predicate(random_variable/3, public).
dynamic_predicate(random_variable/4, public).
dynamic_predicate(random_variable/5, public).
dynamic_predicate(random_variable/6, public).
dynamic_predicate(random_variable/7, public).
dynamic_predicate(random_variable/8, public).
dynamic_predicate(random_constraint/3, public).
dynamic_predicate(random_range_element/2, public).

dynamic_predicate(ndl_constraint_pairs/2, private).
dynamic_predicate(ndl_variable_definition/3, private).
dynamic_predicate(ndl_type_use/2, private).
dynamic_predicate(ndl_range_element/2, private).
dynamic_predicate(ndl_serialize_target/1, private).
dynamic_predicate(ndl_namespace/1, private).
dynamic_predicate(ndl_debug/1, private).

ndl_model(Options, Model) :-
    init(Options),
    call_once(Model),
    finalize().
ndl_model(Model) :-
    ndl_model(_{}, Model).

call_once(Model) :-
    conjuct_to_list(Model, ModelList),
    call_once_list(ModelList).
call_once_list([]).
call_once_list([H | Rest]) :-
    once(H),
    call_once_list(Rest).    

init(Options) :-
    (get_dict(namespace, Options, Namespace) -> true; Namespace = user),
    (get_dict(debug, Options, Debug) -> true; Debug = false),
    (get_dict(serialize, Options, Serialize) -> true; Serialize = []),
    remember(ndl_namespace(Namespace)),
    remember(ndl_debug(Debug)),
    remember(ndl_serialize_target(Serialize)), 
    findall(Predicate, dynamic_predicate(Predicate, public), Predicates),
    compile_predicates(Predicates),
    forall(dynamic_predicate(Predicate, public), dynamic(Namespace:Predicate)).
init() :-
    init(_{}).

finalize() :-
    compile_type_uses(),
    compile_predicates_list(),
    compile_reified_constraints(),
    ((ndl_serialize_target(Name), Name \= []) -> serialize(Name); compile()).

compile_predicates_list() :-
    ndl_namespace(Namespace),
    forall((
            internal_body_predicate(Predicate/Arity),
            functor(Check, Predicate, Arity),
            once(Namespace:Check)
        ),
        remember(body_predicate(Predicate/Arity))
    ),
    forall((
            Namespace:constant_definition(ConstInteger, Value), 
            Value \= _.._),
        remember(available_integer(ConstInteger))
    ),
    forall(Namespace:constant_definition(ConstRange, _.._), 
        remember(available_range(ConstRange))),
    forall((ndl_variable_definition(VariableName, Indexing, _),
            length(Indexing, Arity)),
        remember(available_variable(VariableName/Arity))),
    forall(ndl_constraint_pairs(ConstraintName, _), 
        remember(available_constraint(ConstraintName))).

compile_type_uses() :-
    \+ ndl_type_use(_, _), !.
compile_type_uses() :-  
    setof(T, U^(ndl_type_use(T, U)), Types),
    forall(member(T, Types), (
        setof(U, ndl_type_use(T, U), Uses),
        remember(type_uses(T, Uses)))
    ),
    compile_const_types().

compile_const_types() :-
    forall(type_uses(const(C), Uses), (
        uses_to_types(Uses, TypesList),
        sort(TypesList, TypesSet),
        forall(member(Type, TypesSet), 
            remember(type_of(const(C), Type))
        )
    )).

uses_to_types([], []).
uses_to_types([H|T],[NH|NT]) :-
    type_of(H, NH), !,
    uses_to_types(T, NT).
uses_to_types([_|T],NT) :-
    uses_to_types(T, NT).

compile_reified_constraints() :-
    forall(reified(Name), (
        ndl_constraint_pairs(Name, Pairs),
        forall(nth1(Index, Pairs, Var1-Var2), (
            Var =.. [Name, Index],
            Fact =.. [reified_constraint, Name, Var1-Var2, Var],
            remember(Fact)
        ))
    )).

compile() :-
    (ndl_debug(false) ->
        (forall(dynamic_predicate(PrivatePredicate/Arity, private),
        (
            forget(PrivatePredicate/Arity))
        )
    )).

serialize(Name) :- 
    ndl_namespace(Namespace),
    atom_concat(Name, '.ndl', Filename),
    tell(Filename),
    (ndl_debug(true) ->
        (
            writef('\n%% Debug \n'),
            forall(dynamic_predicate(Predicate/Arity, private), 
            listing(Predicate/Arity))
        ); true
    ),
    writef('%% Head\n'),
    writef('% Model: %w\n', [Name]), 
    writeln(':- use_module(library(noodle)).'),
    findall(Predicate/Arity, 
        (
            dynamic_predicate(Predicate/Arity, public)
        ),
        NonEmptyPredicates
    ),
    include(body_predicate, NonEmptyPredicates, BodyPredicates),
    include(header_predicate, NonEmptyPredicates, HeaderPredicates),
    compile(),
    forall((member(Predicate, HeaderPredicates)), 
        listing(Namespace:Predicate)
    ),
    writef('\n%% Body\n'),
    forall((member(Predicate, BodyPredicates)), 
        listing(Namespace:Predicate)
    ),
    told.

define_type(Context, Type) :-
    remember(type_of(Context, Type)).
define_constant(Name, _) :-
    ndl_namespace(Namespace),
    Namespace:constant_definition(Name, _),
    !.
define_constant(Name, Constant used_in Usages) :-
    !, 
    remember(constant_definition(Name, Constant)),
    remember(constant(Name, Constant)),
    forall(member(Use, Usages),
        (remember(ndl_type_use(const(Name), Use)))    
    ).
define_constant(Name, Value) :-
    define_constant(Name, Value used_in []).

define_range(Name, _) :-
    ndl_namespace(Namespace),
    Namespace:range_definition(Name, _),
    !.
define_range(Name, ConstFrom..ConstTo used_in Usages) :-
    !,
    get_constant_value(ConstFrom, [], From),
    get_constant_value(ConstTo, [], To),
    define_range_element(Name, ConstFrom used_in Usages),
    define_range_element(Name, ConstTo used_in Usages), 
    selector(exhaustive, From, To, Value, Getter),
    selector(random, From, To, Value, RandomGetter),
    remember(range_element(Name, Value) :- Getter),
    remember(random_range_element(Name, Value) :- RandomGetter),
    remember(range_definition(Name, From..To)),
    forall(member(Use, Usages),
        remember(ndl_type_use(range(Name), Use))    
    ).
define_range(Name, Value) :-
    define_range(Name, Value used_in []).

define_range_element(Name, const(Constant) used_in Usages) :-
    !, 
    use_constant(Constant, Usages, _),
    remember(ndl_range_element(Name, Constant)).
define_range_element(Name, Value used_in Usages) :-
    atomic_list_concat([Name, '_', el, '_'], ElementNameBase),
    gensym(ElementNameBase, ElementName),
    define_constant(ElementName, Value used_in Usages),
    remember(ndl_range_element(Name, ElementName)).

define_constraint_extension(Name, Pairs) :- 
    is_list(Pairs),
    remember(ndl_constraint_pairs(Name, Pairs)),
    forall(member(Var1-Var2, Pairs), 
        remember(constraint(Name, Var1, Var2))
    ),
    remember((random_constraint(Name, Var1, Var2) :- 
        random_permutation(Pairs, Permutation),
        member(Var1-Var2, Permutation)
    )).

define_constraint(Name, Type1-Type2, Pairs) :-
    define_constraint_extension(Name, Pairs),
    define_constraint_argument_type(Name, 1, Type1),
    define_constraint_argument_type(Name, 2, Type2).

define_constraint_argument_type(Name, Index, var(Variable)) :-
    !,
    ndl_variable_definition(Variable, _, _),
    Context = arg(Name, Index),
    Type = var(Variable),
    define_type(Context, Type),
    remember(ndl_type_use(Type, Context)).
define_constraint_argument_type(Name, Index, range(Type)) :-
    !,
    Context = arg(Name, Index),
    use_constant(Type, [Context], _),
    define_type(Context, range(Type)).
define_constraint_argument_type(Name, Index, Value) :-
    atomic_list_concat([Name, '_arg_', Index], ConstName),
    define_constant(ConstName, Value),
    define_constraint_argument_type(Name, Index, range(ConstName)). 

define_constraint_semantics((Head :- Body)) :-
    Head =.. [Name, _, _],
    remember(constraint_semantics(Name, (Head :- Body))).

reify_constraint(Name) :-
    ndl_constraint_pairs(Name, Pairs),
    length(Pairs, N),
    define_range(boolean, 0..1),
    define_variable(Name, [1..N], range(boolean)),
    remember(reified(Name)).

get_reified_variable(Name, Var1-Var2, Var) :-
    ndl_constraint_pairs(Name, Pairs),
    nth1(Index, Pairs, Var1-Var2),
    Var =.. [Name, Index].

fix_variable(Variable) :-
    ndl_variable_definition(Variable, _, _),
    remember(fixed(Variable)).

make_variable_depend_on(Variable, Type, Pairs) :-
    ndl_variable_definition(Variable, _, _),
    remember(auxillary(Variable, Type)),
    forall(member(Var-Dep, Pairs), 
        remember(depends(Variable, Var, Dep))).

define_dependency_semantics((Head :- Body)) :-
    Head =.. [Name, _, _, _],
    auxillary(Name, _),
    remember(auxillary_semantics(Name, (Head :- Body))).

define_variable(Name, Indexing, Domain) :-
    remember(ndl_variable_definition(Name, Indexing, Domain)),
    assert_var_domain(Name, Domain),
    assert_var_definition(Name, Indexing).

assert_var_domain(Name, range(ConstName)) :-
    get_constant_value(range(ConstName), [domain(Name)], _),
    define_type(domain(Name), range(ConstName)).
assert_var_domain(Name, From..To) :-
    (atom_concat(Name, '_domain', ConstName),
    define_range(ConstName, From..To used_in [domain(Name)])),
    define_type(domain(Name), range(ConstName)).

assert_var_definition(Name, Indexing) :- 
    length(Indexing, VarArity),
    var_term(Name, VarArity, VarTerm),
    var_def_head(variable, Name, VarTerm, VarDefHead),
    var_def_body(Name, VarDefHead, Indexing, VarDefBody, exhaustive),
    remember(VarDefHead :- VarDefBody),
    var_def_head(random_variable, Name, VarTerm, VarDefHeadRandom),
    var_def_body(Name, VarDefHeadRandom, Indexing, VarDefBodyRandom, random),
    remember(VarDefHeadRandom :- VarDefBodyRandom).

var_term(Name, Arity, VarTerm) :-
    compound_name_arity(VarTerm, Name, Arity).

var_def_head(PredName, Name, VarTerm, VarDefHead) :-
    term_variables(VarTerm, VarIndexes),
    append([PredName, Name | VarIndexes], [VarTerm], HeadAtomList),
    VarDefHead =.. HeadAtomList.

var_def_body(Name, VarDefHead, Indexing, VarDefBody, Mode) :-
    term_variables(VarDefHead, VarIndexes),
    pairs_keys_values(AtomParts, VarIndexes, Indexing),
    var_def_body_atoms_to_indexing(Name, Mode, AtomParts, VarDefBody).

var_def_body_atoms_to_indexing(Name, Mode, AtomsList, Body) :-
    var_def_body_atoms_to_indexing(Name, Mode, 1, AtomsList, Body).
var_def_body_atoms_to_indexing(Name, Mode, Index, [Var-Range], Body) :- 
    !,
    var_def_index_range(Name, Index, Range, From..To),
    selector(Mode, From, To, Var, Body).
var_def_body_atoms_to_indexing(Name, Mode, Index, [Var-Range|AtomsRest], (Body, BodyRest)) :- 
    var_def_index_range(Name, Index, Range, From..To),
    selector(Mode, From, To, Var, Body),
    NewIndex is Index + 1,
    var_def_body_atoms_to_indexing(Name, Mode, NewIndex, AtomsRest, BodyRest).

var_def_index_range(Name, Index, range(Range), From..To) :-
    !,
    get_constant_value(range(Range), [index(Name,Index)], From..To),
    define_type(index(Name,Index), range(Range)).

var_def_index_range(Name, Index, Range, From..To) :- 
    get_constant_value(Range, [index(Name,Index)], From..To),
    atomic_list_concat([Name, '_index_', Index], RangeName),
    define_range(RangeName, From..To used_in [index_range(Name,Index)]),
    define_type(index(Name,Index), range(RangeName)).

get_constant_value(Constant, _, Constant) :-
    integer(Constant).
get_constant_value(From..To, Context, FromValue..ToValue) :-
    get_constant_value(From, Context, FromValue),
    get_constant_value(To, Context, ToValue).
get_constant_value(const(Constant), Context, Value) :-
    use_constant(Constant, Context, Dereferenced),
    get_constant_value(Dereferenced, Context, Value).
get_constant_value(range(Range), Context, Value) :-
    use_constant(Range, Context, Dereferenced),
    get_constant_value(Dereferenced, Context, Value).

use_constant(Constant, Context, Value) :-
    ndl_namespace(Namespace),
    Namespace:constant_definition(Constant, Value),
    forall(member(C, Context), 
        remember(ndl_type_use(const(Constant), C))
    ).

use_constant(Range, Context, Value) :-
    ndl_namespace(Namespace),
    Namespace:range_definition(Range, Value),
    forall(member(C, Context), (
        remember(ndl_type_use(range(Range), C)),
        forall(ndl_range_element(Range, B),
            remember(ndl_type_use(const(B), C))
        )
    )).

selector(random, From, To, Val, Selector) :- 
    Selector = (
            findall(V, between(From, To, V), Vals),
            random_permutation(Vals, Permutation),
            member(Val, Permutation)
    ).

selector(exhaustive, From, To, Val, Selector) :- Selector = between(From, To, Val).

remember(X :- Y) :-
    !,
    functor(X, Name, Arity),
    (dynamic_predicate(Name/Arity, public) ->
        public_assert(X :- Y);
        assert(X :- Y)).
remember(X) :-
    functor(X, Name, Arity),
    (dynamic_predicate(Name/Arity, public) ->
        (public_once(X) -> true; public_assert(X));
        (once(X) -> true; assert(X))).


forget(Name/Arity) :- !,
    functor(X, Name, Arity),
    forget(X).
forget(X) :-
    functor(X, Name, Arity),
    (dynamic_predicate(Name/Arity, public) ->
        public_retractall(X);
        retractall(X)).

public_assert(X) :- 
    ndl_namespace(Namespace),
    Namespace:assert(X).
public_retractall(X) :-
    ndl_namespace(Namespace),
    Namespace:retractall(X).
public_call(X) :-
    ndl_namespace(Namespace),
    Namespace:call(X).
public_once(X) :-
    ndl_namespace(Namespace),
    Namespace:once(X).