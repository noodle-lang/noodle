#!/bin/bash

declare -a examples=(
    "graph-coloring-kempe-memory" 
    "graph-coloring-kempe-recursive"
    "graph-coloring-kempe-semantic" 
    "nqueens-horizontal-symmetry" 
    "traveling-tournament-partial-swap-teams" 
    "traveling-tournament-partial-swap-teams-semantic"
    "traveling-tournament-swap-teams" 
    "traveling-tournament-swap-rounds" 
    "tsp-2opt" 
    "tsp-2swap" 
    "tsp-2opt-memory"
    "tsp-2opt-safe"
    "semantic-variables"
    )

## now loop through the above array
for i in "${examples[@]}"
do
  ./run_example.sh "${i}" 
   # or do whatever with individual element of the array
done