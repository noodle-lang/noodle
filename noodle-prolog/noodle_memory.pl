:- module(noodle_memory, [
        ndl_memory/1,
        ndl_memory_remember/3,
        ndl_memory_contains/2
    ]).
    
    :- use_module(library(assoc)).
    
    ndl_memory(M) :- 
        empty_assoc(M).
    
    ndl_memory_remember(M, R, NM) :-
        put_assoc(R, M, t, NM).
        
    ndl_memory_contains(M, R) :-
        get_assoc(R, M, _).
