:- module(noodle_state, [
        ndl_state_init/2,
        ndl_state_empty/1,
        ndl_state_just_solution/2,
        ndl_state_new/2,
        ndl_state_set_solution/3,
        ndl_state_get_solution/2,
        ndl_state_get_stack/4,
        ndl_state_set_stack/4,
        ndl_state_get_avar/2,
        ndl_state_set_avar/3,
        ndl_state_get_cstore/2,
        ndl_state_set_cstore/3,
        ndl_state_get_memory/2,
        ndl_state_set_memory/3
    ]).

:- use_module(library(assoc)).
:- use_module(noodle_stack).
:- use_module(noodle_constraint_store).
:- use_module(noodle_avar_store).
:- use_module(noodle_memory).

:- reexport([
        noodle_stack,
        noodle_avar_store,
        noodle_constraint_store,
        noodle_memory
    ]).

:- dynamic(ndl_semantics/1).
:- dynamic(ndl_namespace/1).

ndl_state_init(Namespace, Semantics) :-
    retractall(ndl_semantics(_)),
    retractall(ndl_namespace(_)),
    assert(ndl_semantics(Semantics)),
    assert(ndl_namespace(Namespace)).    

ndl_state_empty(state{solution:_, stacks:_, memory:_}) :-
    ndl_semantics(read).
ndl_state_empty(state{solution:_, stacks:_, memory:_, cstore:C, avar:A}) :-
    ndl_cstore_empty(C),
    ndl_avar_empty(A),
    ndl_semantics(write).

ndl_state_just_solution(Solution, State) :-
    ndl_state_empty(State),
    State.solution = Solution.

ndl_state_new(Solution, FullState) :-
    ndl_state_just_solution(Solution, State),
    empty_assoc(State.stacks),
    ndl_memory(State.memory),
    fill_semantics(State, FullState).

fill_semantics(state{solution:_, stacks:_, memory:_}, state{solution:_, stacks:_, memory:_}).
fill_semantics(State, FullState) :-
    ndl_namespace(N),
    ndl_avar_new(N, State, Avar),
    ndl_state_set_avar(State, Avar, AvarState),
    ndl_cstore_new(N, AvarState, CStore),
    ndl_state_set_cstore(AvarState, CStore, FullState).

ndl_state_get_solution(State, Solution) :-
    get_dict(solution, State, Solution).
ndl_state_set_solution(State, Solution, NewState) :-
    put_dict(solution, State, Solution, NewState).
ndl_state_get_stack(State, Name, Stack, State) :-
    get_assoc(Name, State.stacks, Stack), !.
ndl_state_get_stack(State, Name, Stack, NewState) :-
    ndl_stack_empty(Stack),
    ndl_state_set_stack(State, Name, Stack, NewState).
ndl_state_set_stack(State, Name, Stack, NewState) :-
    put_assoc(Name, State.stacks, Stack, NewStacks),
    put_dict(stacks, State, NewStacks, NewState).
ndl_state_get_cstore(State, CStore) :-
    get_dict(cstore, State, CStore).
ndl_state_set_cstore(State, CStore, NewState) :-
    put_dict(cstore, State, CStore, NewState).
ndl_state_get_avar(State, AVar) :-
    get_dict(avar, State, AVar).
ndl_state_set_avar(State, AVar, NewState) :-
    put_dict(avar, State, AVar, NewState).
ndl_state_get_memory(State, Memory) :-
    get_dict(memory, State, Memory).
ndl_state_set_memory(State, Memory, NewState) :-
    put_dict(memory, State, Memory, NewState).

