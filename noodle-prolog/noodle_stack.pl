:- module(noodle_stack, [
        ndl_stack_empty/1,
        ndl_stack_push/3,
        ndl_stack_pop/3,
        ndl_stack_top/2,
        ndl_stack_contains/2
    ]).
    
ndl_stack_empty([]).
ndl_stack_push(Stack, El, [El|Stack]).
ndl_stack_pop([El|Stack], El, Stack).
ndl_stack_top([El|_], El).
ndl_stack_contains(Stack, El) :- member(El, Stack).