:- use_module(noodle_model).
:- begin_tests(noodle_model).

test(define_constant) :-
    ndl_model((
        define_constant(constant, 1)
    )),
    assertion(constant(constant, 1)),
    assertion(constant_definition(constant, 1)).

:- end_tests(noodle_model).