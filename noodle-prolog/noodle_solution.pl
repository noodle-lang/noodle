:- module(noodle_solution, [
    ndl_is_solution/1,
    ndl_solution/2,
    ndl_solution_to_list/2,
    ndl_set_value/4,
    ndl_swap_values/4,
    ndl_flip_variable/5,
    ndl_get_value/3,
    ndl_gen_value/3,
    ndl_diff/3
]).

:- use_module(library(assoc)).

ndl_solution(VarsToValues, Solution) :- 
    list_to_assoc(VarsToValues, Solution).

ndl_solution_to_list(Solution, List) :-
    assoc_to_list(Solution, List).

ndl_is_solution(Solution) :-
    is_assoc(Solution).  

ndl_get_value(Solution, Variable, Value) :-
    get_assoc(Variable, Solution, Value).
    
ndl_set_value(OldSolution, Variable, Value, NewSolution) :-
    get_assoc(Variable, OldSolution, _),
    put_assoc(Variable, OldSolution, Value, NewSolution).

ndl_gen_value(Solution, Variable, Value) :-
    gen_assoc(Variable, Solution, Value).

ndl_swap_values(OldSolution, Var1, Var2, NewSolution) :-
    ndl_get_value(OldSolution, Var1, Val1),
    ndl_get_value(OldSolution, Var2, Val2),
    ndl_set_value(OldSolution, Var1, Val2, Solution),
    ndl_set_value(Solution, Var2, Val1, NewSolution).

ndl_flip_variable(OldSolution, Var1, Val1, Val2, NewSolution) :-
    ndl_get_value(OldSolution, Var1, Val1), !,
    ndl_set_value(OldSolution, Var1, Val2, NewSolution).
ndl_flip_variable(OldSolution, Var1, Val1, Val2, NewSolution) :-
    ndl_get_value(OldSolution, Var1, Val2), !,
    ndl_set_value(OldSolution, Var1, Val1, NewSolution).

ndl_diff(OldSolution, NewSolution, Diff) :-
    assoc_to_keys(OldSolution, Keys),
    ndl_diff(OldSolution, Keys, NewSolution, Diff).
    
ndl_diff(_, [], _, []).
ndl_diff(OldSolution, [Key|Rest], NewSolution, Diff) :-
    ndl_get_value(OldSolution, Key, OldValue),
    ndl_get_value(NewSolution, Key, NewValue),
    (OldValue = NewValue ->
        Diff = NRest;
        Diff = [Key-NewValue|NRest]
    ),
    ndl_diff(OldSolution, Rest, NewSolution, NRest).
    