#!/bin/sh
PROLOG=swipl

$PROLOG -g run -g halt -f "examples/${1}.pl"
echo "${1}: Check created files in the 'build' folder"