:- module(noodle_utils, [
    list_to_conjuct/2,
    conjuct_to_list/2,
    safe_div/3,
    safe_fdiv/3,
    semirandom_between/3,
    semirandom_member/2,
    is_any_list/1,
    list_mean/2,
    list_variance/2,
    list_population_standard_deviation/2,
    list_of_counters/2,
    list_increment_counter/3
]).

is_any_list([]).
is_any_list([_|_]).

safe_div(_, 0, 0) :- !.
safe_div(X, Y, Z) :-
    Z is X // Y.

safe_fdiv(_, 0, 0.0) :- !.
safe_fdiv(X, Y, Z) :-
    Z is X / Y.

conjuct_to_list((H,Rest), List) :-
    !,
    conjuct_to_list(H, NH),
    conjuct_to_list(Rest, NRest),
    append(NH, NRest, List).
conjuct_to_list(A, [A]).

list_to_conjuct([], (true)).
list_to_conjuct([A], (A)) :- !.
list_to_conjuct([A,B|Rest], (A, NRest)) :-
    list_to_conjuct([B|Rest], NRest).

semirandom_member(X, List) :-
    nonvar(X), !, member(X, List), !.
semirandom_member(X, List) :- random_member(X, List).

semirandom_between(From, To, X) :-
    nonvar(X), !, between(From, To, X).
semirandom_between(From, To, X) :- random_between(From, To, X).

list_mean(List, Mean) :-
	list_length_mean(List, _Len, Mean).

list_length_mean(List, Len, Mean) :-
	length(List, Len),
	(   Len == 0
	->  Mean is 0
	;   sumlist(List, Sum),
	    Mean is Sum / Len
	).

list_variance(List, Variance) :-
	list_mean(List, Mean),
	variance(List, Mean, 0, Variance).

variance([], _, V, V).
variance([H|T], Mean, V0, V) :-
	V1 is V0+(H-Mean)**2,
	variance(T, Mean, V1, V).

list_population_standard_deviation(List, StDev) :-
	list_length_mean(List, Len, Mean),
	variance(List, Mean, 0, Variance),
    StDev is sqrt(Variance / Len).

list_of_counters([], []) :- !.
list_of_counters([Name|TNames], [Name-0|R]) :-
    list_of_counters(TNames, R).

list_increment_counter([OName-OC|Rest], Name, [OName-NC|Rest]) :- 
    OName == Name,
    !,
    NC is OC + 1.
list_increment_counter([OC|ORest], Name, [OC|NRest]) :-
    list_increment_counter(ORest, Name, NRest).


:- begin_tests(noodle_utils_unit).
    
test(l2c_empty) :- list_to_conjuct([], (true)).
test(l2c_single) :- list_to_conjuct([a(b,c)], (a(b,c))).
test(l2c_multiple) :-
    list_to_conjuct(
        [a(b,c), z(X,c), z(c,X)],
        (a(b,c), z(X,c), z(c,X))
    ).

test(c2l_single) :- conjuct_to_list((a(b,c)), [a(b,c)]).
test(c2l_multiple) :-
    conjuct_to_list(
        (a(b,c), z(X,c), z(c,X)),
        [a(b,c), z(X,c), z(c,X)]
    ).
test(c2l_deep) :-
    conjuct_to_list(
        ((a(b,c), (z(X,c))), z(c,X)),
        [a(b,c), z(X,c), z(c,X)]
    ).

:- end_tests(noodle_utils_unit).
