:- module(noodle_avar_store, [
    ndl_avar_empty/1,
    ndl_avar_new/3,
    ndl_avar_get/3,
    ndl_avar_gen/3,
    ndl_avar_update/4
]).

:- use_module(noodle_state).
:- use_module(library(assoc)).

ndl_avar_empty(_).

ndl_avar_new(N, State, FullStore) :-
    findall(Name-Var-D, (
        N:depends(Name, Var, D)
    ), Dependencies),
    reverse(Dependencies, RDependencies),
    fill_store(N, State, RDependencies, FullStore, _).

fill_store(_, State, [], S, State) :- empty_assoc(S).
fill_store(N, State, [Name-Var-Dep|R], NS, NState) :-
    fill_store(N, State, R, S, TState),
    (N:get_aux_value(Name, TState, Var, Dep, Val) ->
        ndl_avar_update(S, Var, Val, NS)
        ;
        NS = S
    ),
    ndl_state_set_avar(TState, NS, NState).


ndl_avar_get(Avar, Variable, Value) :-
    get_assoc(Variable, Avar, Value).

ndl_avar_gen(Avar, Variable, Value) :-
    gen_assoc(Variable, Avar, Value).

ndl_avar_update(Avar, Var, Val, NAvar) :-
    put_assoc(Var, Avar, Val, NAvar).
