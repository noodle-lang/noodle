:- module(noodle_propagatr, [
        ndl_set_value_and_propagate/4,
        ndl_swap_values_and_propagate/4,
        ndl_flip_variable_and_propagate/5
    ]).

:- use_module(noodle).

ndl_set_value_and_propagate(OldState, Var, Val, NewState) :-
    ndl_state_get_solution(OldState, OldSol),
    ndl_set_value(OldSol, Var, Val, NewSol),
    ndl_state_set_solution(OldState, NewSol, TempState),
    ndl_propagate([Var], TempState, NewState).
ndl_swap_values_and_propagate(OldState, Var1, Var2, NewState) :-
    ndl_state_get_solution(OldState, OldSol),
    ndl_swap_values(OldSol, Var1, Var2, NewSol),
    ndl_state_set_solution(OldState, NewSol, TempState),
    ndl_propagate([Var1, Var2], TempState, NewState).
ndl_flip_variable_and_propagate(OldState, Var, Val1, Val2, NewState) :-
    ndl_state_get_solution(OldState, OldSol),
    ndl_flip_variable(OldSol, Var, Val1, Val2, NewSol),
    ndl_state_set_solution(OldState, NewSol, TempState),
    ndl_propagate([Var], TempState, NewState).

ndl_propagate(Changes, State, NState) :-
    ndl_propagate(Changes, State, NState, user, []).

ndl_propagate([], State, State, _, _).
ndl_propagate([Change | Rest], State, NState, N, Acc) :-
    member(Change, Acc), !,
    ndl_propagate(Rest, State, NState, N, Acc).
ndl_propagate([constraint(Name, Var1, Var2) | Rest], State, NState, N, Acc) :- 
    N:reified_constraint(Name, Var1-Var2, Change), !,
    ndl_propagate([Change | Rest], State, NState, N, [constraint(Name, Var1, Var2) | Acc]).
ndl_propagate([constraint(Name, Var1, Var2) | Rest], State, NState, N, Acc) :- 
    !,
    ndl_propagate(Rest, State, NState, N, [constraint(Name, Var1, Var2) | Acc]).
ndl_propagate([Var | Rest], State, NState, N, Acc) :-
    findall(Dep, N:depends(_, Dep, Var), Deps),
    findall(constraint(Name, Var, Var2), N:constraint(Name, Var, Var2), Constr1s),
    findall(constraint(Name, Var2, Var), N:constraint(Name, Var2, Var), Constr2s),
    append(Constr1s, Constr2s, Constr),
    reverse(Deps, RDeps),
    update_avar(RDeps, State, AVarState, N),
    update_cstore(Constr, AVarState, CStoreState, N),
    append([Deps, Constr, Rest], Changes), !,
    ndl_propagate(Changes, CStoreState, NState, N, [Var | Acc]).

update_avar([], State, State, _).
update_avar([Var | Rest], State, NState, N) :-
    update_avar(Rest, State, TState, N),
    findall(Dep, N:depends(Name, Var, Dep), Deps),
    update_avar_(Name, Var, Deps, TState, NState, N).

update_avar_(_, _, [], State, State, _).
update_avar_(Name, Var, [Dep|Rest], State, NState, N) :-  
    ( N:get_aux_value(Name, State, Var, Dep, Val) ->
        ndl_state_get_avar(State, AVar),
        ndl_avar_update(AVar, Var, Val, NAVar),
        ndl_state_set_avar(State, NAVar, TState)
        ;
        State = TState
    ),
    update_avar_(Name, Var, Rest, TState, NState, N).
    

update_cstore([], State, State, _).
update_cstore([constraint(Name, Arg1, Arg2) | Rest], State, NState, N) :-
    N:is_satisfied(Name, State, Arg1, Arg2), !,
    ndl_state_get_cstore(State, CStore),
    ndl_cstore_update_sat(CStore, constraint(Name, Arg1, Arg2), TCStore),
    ndl_state_set_cstore(State, TCStore, TState),
    update_cstore(Rest, TState, NState, N).
update_cstore([C | Rest], State, NState, N) :-
    ndl_state_get_cstore(State, CStore),
    ndl_cstore_update_unsat(CStore, C, TCStore),
    ndl_state_set_cstore(State, TCStore, TState),
    update_cstore(Rest, TState, NState, N).