#!/usr/bin/env swipl
:- use_module(library(noodle)).
:- consult('gc-domain.ndl').

:- initialization(main, main).

main([Path]) :-
    read(NQL),
    ndl_query(_{serialize:Path, semantics:write}, [
            NQL   
        ]).
    