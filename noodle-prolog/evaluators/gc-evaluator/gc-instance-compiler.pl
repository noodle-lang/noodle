#!/usr/bin/env swipl
:- use_module(library(clpfd)).
:- use_module(library(noodle)).

:- initialization(main, main).

main([GraphFile, DomainTarget, SolutionsTarget, Max]) :- 
main([GraphFile, DomainTarget, SolutionsTarget, Max, r]).

main([GraphFile, DomainTarget, SolutionsTarget, Max, Mode]) :-
    consult(GraphFile),
    create_model(DomainTarget),
    atom_number(Max, MaxNumber),
    create_solutions(SolutionsTarget, MaxNumber, Mode).

graph_edge(N1, N2) :-
    edge(N1, N2).
graph_edge(N1, N2) :-
    edge(N2, N1).

create_model(Target) :- 
    nodes_number(NodesNumber),
    N is NodesNumber - 1,  
    ndl_model(_{serialize:Target}, (
        define_range(indexes, 0..N),
        define_range(colors, 0..N),
        define_variable(nodes, [range(indexes)], range(colors)),
        findall(Node1-Node2, (
            graph_edge(N1,N2), 
            variable(nodes, N1, Node1), 
            variable(nodes, N2, Node2)), 
        Edges),
        define_constraint(diff, var(nodes)-var(nodes), Edges),
        define_constraint_semantics((diff(Var1, Var2) :- get_value(Var1, Val1), get_value(Var2, Val2), Val1 \= Val2))
    )).

create_labeling(Solution) :- create_labeling(Solution, 0).
create_labeling(Solution, Seed) :- once(labeling([random_variable(Seed), random_value(Seed)], Solution)).
create_labeling(Solution, Seed) :- NewSeed is Seed + 1, create_labeling(Solution, NewSeed).

create_solution(Ns, Sol) :- 
    findall(N-V, (
            variable(nodes, I, N),
            nth0(I, Ns, V)
            ), 
            VarVals
        ),
        ndl_solution(VarVals, Sol).

create_solutions(Target, Max, Mode) :-
    nodes_number(NodesNumber), 
    graph_coloring(NodesNumber, Ns, Max, Mode),
    SolN = 10,
    append(Target),
    (Mode = 'r' -> (
            findnsols(SolN, Solution, (
                create_labeling(Ns),
                create_solution(Ns, Solution)
            ), Solutions),     
            forall(nth1(I, Solutions, S), format('solution(~w, ~w).\n', [I,S]))
        ); (
            labeling([ff], Ns),
            create_solution(Ns, Solution),
            format('solution(~w, ~w).\n', [o,Solution])
        )
    ),  
    told.

graph_coloring(N, Nodes, Max, Mode) :-
    length(Nodes, N),
    clique(C),
    length(C,CLength),
    (Mode = 'r' ->
        RelaxedMax is Max + 1;
        RelaxedMax is Max - 1),
    Nodes ins 0..RelaxedMax,
    colored_nodes(0, N, Nodes),
    fix_clique(0, CLength, C, Nodes).

colored_nodes(I, I, _).
colored_nodes(I, N, Nodes) :-
    I < N, 
    findall(I-I2, graph_edge(I,I2), Edges),
    edges(Nodes, Edges),
    NI is I + 1,
    colored_nodes(NI, N, Nodes).
edges(_, []).
edges(Nodes, [I1-I2|Es]) :-
    nth0(I1, Nodes, N1),
    nth0(I2, Nodes, N2),
    N1 #\= N2,
    edges(Nodes, Es).
fix_clique(I, I, _, _).
fix_clique(I, L, Clique, Nodes) :-
    I < L,
    nth0(I, Clique, C),
    nth0(C, Nodes, N),
    N #= I,
    NI is I + 1,
    fix_clique(NI, L, Clique, Nodes).