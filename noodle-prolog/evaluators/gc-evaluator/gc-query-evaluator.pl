#!/usr/bin/env swipl
:- use_module(library(noodle)).
:- use_module(library(yall)).
:- use_module(library(http/json)).
:- use_module(library(occurs)).

:- initialization(main, main).

:- dynamic(constraint_amount/2).
:- dynamic(solution_size/1).
:- dynamic(neighbors_total/2).
:- dynamic(valid_total/2).
:- dynamic(instance_name/1).
:- dynamic(cost_total/2).
:- dynamic(cost_min/2).
:- dynamic(cost_max/2).
:- dynamic(changes_total/2).
:- dynamic(changes_min/2).
:- dynamic(changes_max/2).
:- dynamic(satisfied_total/3).
:- dynamic(satisfied_min/3).
:- dynamic(satisfied_max/3).

main([QueryName, InstanceName, NSolutionsString]) :-
    atomic_list_concat([QueryName, '.nql'], QueryPath),
    atomic_list_concat(['instances/', InstanceName, '.ndl'], DomainPath),
    atomic_list_concat(['instances/', InstanceName, '.nsl'], SolutionsPath),
    term_to_atom(NSolutions, NSolutionsString),
    consult(DomainPath),
    consult(SolutionsPath),
    consult(QueryPath),
    assert(instance_name(InstanceName)),
    MaxSolIndex is NSolutions - 1,
    calculate_instance_stats(),
    calculate_scores(MaxSolIndex),
    create_outputs(MaxSolIndex, Outputs),
    current_output(Stream),
    json_write(Stream, Outputs).

create_outputs(MaxSolIndex, Outputs) :-
    findall(Output, ( 
        between(0, MaxSolIndex, Index),
        create_output(Index, Output)
    ), Outputs).

create_output(Index, output{
        instance: Instance,
        neighbors: Score
    }) :-
        create_instance(Index, Instance),
        create_score(Index, Score).

create_instance(Index, instance{
        constraints: Constraints, 
        name: Name,
        solution: Index,
        variables: Variables
    }) :-
        findall(C-Count, (   
                available_constraint(C),
                constraint_amount(C, Count)
            ),
            ConstraintsList 
        ),
        dict_create(Constraints, constraints, ConstraintsList),
        instance_name(Name),
        solution_size(Variables).
create_score(Index, 
      neighbors{
        amount:Amount,
        valid:Valid,
        changes: Changes,
        satisfied: Satisfied, 
        cost: Cost}) :-
    neighbors_total(Index, Amount),
    valid_total(Index, Valid),
    get_stats([cost, Index], Amount, Cost),
    get_stats([changes, Index], Amount, Changes),
    findall(C-Stats, 
        (   
            available_constraint(C),
            get_stats([satisfied, Index, C], Amount, Stats)
        ),
        SatisfiedList 
    ),
    dict_create(Satisfied, satisfied, SatisfiedList).

calculate_instance_stats() :- 
    forall(available_constraint(C),  (
            aggregate_all(count, constraint(C, _, _), NConstraints),
            assert(constraint_amount(C, NConstraints))
    )),
    aggregate_all(count, variable(_,_,_), SolutionSize),
    assert(solution_size(SolutionSize)).

calculate_scores(MaxSolIndex) :- 
    forall(between(0, MaxSolIndex, SolutionIndex), (
        solution(SolutionIndex, InitialSolution),
        forall(neighbor_verbose(InitialSolution, EndState),( 
            increment([neighbors_total, SolutionIndex]),
            calculate_changes(EndState, InitialSolution, SolutionIndex),
            calculate_satisfied(EndState, SolutionIndex),
            calculate_cost(EndState, SolutionIndex)
        ))
    )).

calculate_cost(EndState, SolutionIndex) :- 
    ndl_state_get_solution(EndState, Solution),
    cost_function(Solution, Cost),
    update([cost, SolutionIndex], Cost).

cost_function(Solution, Cost) :-
    findall(Val-Var, 
        ndl_gen_value(Solution, Var, Val),
        ValToVarPairs
    ),
    group_pairs_by_key(ValToVarPairs, ColorsToVars),
    maplist([_-Vars, Amount] >> length(Vars, Amount),
        ColorsToVars,
        ColorsAmounts
    ),
    foldl([V, Acc, Res] >> (Res is Acc + V * V), 
        ColorsAmounts, 0, Cost).

calculate_changes(EndState, 
                  InitialSolution,
                  SolutionIndex) :-
    solution_size(SolutionSize),
    ndl_state_get_solution(EndState, EndSolution),
    ndl_diff(InitialSolution, EndSolution, Diff),
    length(Diff, DiffN),
    safe_fdiv(DiffN, SolutionSize, NormalizedDiff),
    IsValid is ceiling(NormalizedDiff),
    update([changes, SolutionIndex], NormalizedDiff),
    increment([valid_total, SolutionIndex], IsValid).
    
calculate_satisfied(EndState, SolutionIndex) :- 
    ndl_state_get_cstore(EndState, CStore),
    forall(available_constraint(C), (
        constraint_amount(C, CCount),
        aggregate_all(count, ndl_cstore_violated(CStore, C, _), TotalError),
        SatisfiedPercentage is 1 - TotalError / CCount,
        update([satisfied, SolutionIndex, C], SatisfiedPercentage)
    )).

get_stats([Name | Args], Amount, 
         stats{
             min_val: MinVal, 
             max_val: MaxVal, 
             avg_val: AvgVal}) :-
    atomic_concat(Name, '_total', TotalName),
    atomic_concat(Name, '_max', MaxName),
    atomic_concat(Name, '_min', MinName),
    create_term([TotalName|Args], TotalValue, TotalTerm),
    create_term([MaxName|Args], MaxVal, MaxTerm),
    create_term([MinName|Args], MinVal, MinTerm),
    call(TotalTerm),
    call(MaxTerm),
    call(MinTerm),
    AvgVal is TotalValue / Amount.
    
update([Name | Args], Amount) :-
    atomic_concat(Name, '_total', TotalName),
    atomic_concat(Name, '_max', MaxName),
    atomic_concat(Name, '_min', MinName),
    increment([TotalName | Args], Amount),
    update_max([MaxName | Args], Amount),
    update_min([MinName | Args], Amount).

increment(Predicate) :-
    increment(Predicate, 1).
increment(Predicate, Amount) :-
    \+ is_list(Predicate), !,
    increment([Predicate], Amount).
increment(Predicate, Amount) :-
    create_term(Predicate, OldAmount, OldTerm),
    (call(OldTerm) ->
        retract(OldTerm),
        NewAmount is OldAmount + Amount,
        create_term(Predicate, NewAmount, NewTerm),
        assert(NewTerm)
        ;
        create_term(Predicate, Amount, NewTerm),
        assert(NewTerm)   
    ).
update_max(Predicate, Value) :-
    \+ is_list(Predicate), !,
    update_max([Predicate], Value).
update_max(Predicate, Value) :-
    create_term(Predicate, OldValue, OldTerm),
    ( call(OldTerm) ->
        NewValue is max(Value, OldValue), 
        retract(OldTerm),
        create_term(Predicate, NewValue, NewTerm),
        assert(NewTerm)
    ;
        create_term(Predicate, Value, NewTerm),
        assert(NewTerm)
    ).
update_min(Predicate, Value) :-
    \+ is_list(Predicate), !,
    update_min([Predicate], Value).
update_min(Predicate, Value) :-
    create_term(Predicate, OldValue, OldTerm),
    ( call(OldTerm) ->
        NewValue is min(Value, OldValue), 
        retract(OldTerm),
        create_term(Predicate, NewValue, NewTerm),
        assert(NewTerm)
    ;
        create_term(Predicate, Value, NewTerm),
        assert(NewTerm)
    ).

create_term(Prefix, Arg, Term) :-
    append(Prefix, [Arg], Args),
    Term =.. Args.

    

    