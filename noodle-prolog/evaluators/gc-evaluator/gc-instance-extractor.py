#!/usr/bin/env python3

import argparse
from typing import List,Tuple
import re
from dataclasses import dataclass
from pathlib import Path
from sultan.api import Sultan
import os

def arg_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser("Simple compiler for the learning data.")
    parser.add_argument("paths", nargs='+', help='paths to knowledge bases') 
    parser.add_argument("--exact", "-e", action='store_true')
    return parser

def main():
    parser = arg_parser()
    args = parser.parse_args()
    paths: List[str] = args.paths
    exact = args.exact
    with Sultan.load() as s:
        for p in paths:
            n_sols = extract_n_solutions(p)
            basename = os.path.splitext(os.path.basename(p))[0]
            if exact:
                s.swipl("gc-instance-compiler.pl {} instances/{} instances/{}.nsl {} exact".format(
                    p, basename, basename, n_sols
                )).run()
            else:
                s.swipl("gc-instance-compiler.pl {} instances/{} instances/{}.nsl {}".format(
                    p, basename, basename, n_sols
                )).run()
            

n_solutions_regexp = re.compile(r'''(\d+)\.pl''')
def extract_n_solutions(path: str) -> int:
    try:
        match = n_solutions_regexp.search(path)
        return match.groups()[0]
    except:
        return -1

if __name__ == "__main__":
    main()