#!/usr/bin/env python3

from typing import Dict, List
from sultan.api import Sultan 
from pathlib import Path
import random
import tempfile 
import sys
import json

kempe_query = '''(neighbor :- constraint(diff, N6, N6), walk_over_inversed(constraint(diff, N5, N3), N3, (flip_variable(N2, C7, C4)))).'''

class GcEvaluator:
    def __init__(self, query: str, cwd: str, states_per_instance: int = 1, num_instances: int = 1, logging: bool = False, clean: bool = True):
        self.cwd = cwd
        self.logging = logging
        self.clean = clean
        self.query = query 
        self.states_per_instance = states_per_instance
        self.num_instances = num_instances
        self.instances = self.get_instances()  
        self.tmpdirname = "{}/{}".format("temp", Path(tempfile.mkdtemp(dir="{}/temp".format(self.cwd))).stem)

    def compile(self, query: str) -> str:
        with Sultan.load(cwd=self.cwd, logging=self.logging) as s:
            query_filepath = "{}/query".format(self.tmpdirname)
            s.echo('"{}"'.format(query)).pipe().swipl("./gc-query-compiler.pl {}".format(query_filepath)).run()
            return query_filepath
    
    def evaluate_code(self) -> Dict:
        with Sultan.load(cwd=self.cwd, logging=self.logging) as s:
            output = s.swipl("./gc-query-analyzer.pl \"{}\"".format(self.query[0:-1])).run()
            if self.logging:
                print(output)
            return json.loads(str(output))

    def evaluate_neighborhood(self, query_filepath: str, instance_name: str) -> List[Dict]:
        with Sultan.load(cwd=self.cwd, logging=self.logging) as s:
            output = s.swipl("./gc-query-evaluator.pl {} {} {}".format(query_filepath, instance_name, self.states_per_instance)).run()
            if self.logging:
                print(output)
            return json.loads(str(output))

    def get_instances(self):
        with Sultan.load(cwd=self.cwd, logging=self.logging) as s:
            instances = s.ls("instances/*.ndl").run()
            instances = list(map(lambda path: Path(path).stem, instances.stdout))
            return instances[0: self.num_instances]
    
    def calculate_code_style_score(self, instance:Dict) -> float:
        risky_part = instance["risky_terms"] / instance["risky_terms_max"]
        risky_score = -2.77*(risky_part**2) + 3.324*risky_part if risky_part < 0.5 else 1.0 
        ignored_outputs_score = None 
        missing_inputs_score = None
        self_loops_score = None
        shadowed_score = None
        effective_score = instance["effective"] / instance["risky_terms_max"]
        n_scores = 2
        total_score = risky_score + effective_score

        if instance["ignored_outputs_max"] > 0:
            ignored_outputs_score = 1 - instance["ignored_outputs"] / instance["ignored_outputs_max"]
            total_score += ignored_outputs_score
            n_scores += 1
        if instance["missing_inputs_max"] > 0:
            missing_inputs_score = 1 - instance["missing_inputs"] / instance["missing_inputs_max"]
            total_score += missing_inputs_score
            n_scores += 1
        if instance["self_loops_max"] > 0:
            self_loops_score = 1 - instance["self_loops"] / instance["self_loops_max"]
            total_score += self_loops_score
            n_scores += 1
        if instance["shadowed_max"] > 0:
            shadowed_score = 1 - instance["shadowed"] / instance["shadowed_max"]
            total_score += shadowed_score
            n_scores += 1

        result = total_score / n_scores
        if self.logging:
            print("\n\t".join(["code_style_score =",
                   "risky_score = {} +".format(risky_score),
                   "ignored_outputs_score = {}".format(ignored_outputs_score),
                   "missing_inputs_score = {}".format(missing_inputs_score),
                   "self_loops_score = {}".format(self_loops_score),
                   "shadowed_score = {}".format(shadowed_score),
                   "effective_score = {}".format(effective_score),
                   "/ {}".format(n_scores),
                   "= {}".format(result)]))
        return result

    def calculate_neighborhood_score(self, instance:Dict) -> float:
        sat_score = sum([s["min_val"] * s["min_val"] for s in instance["neighbors"]["satisfied"].values()]) / len(instance["neighbors"]["satisfied"])
        symmetry_penalty = 1 if instance["neighbors"]["cost"]["min_val"] < instance["neighbors"]["cost"]["max_val"] else 0.5
        changes_prize = instance["neighbors"]["changes"]["avg_val"]
        score = 1 - (1 - sat_score)/changes_prize
        if self.logging:
            print('neighborhood_score = {} * {}'.format(sat_score, instance["neighbors"]["valid"]/instance["neighbors"]["amount"]))
        return score * instance["neighbors"]["valid"]/instance["neighbors"]["amount"] * symmetry_penalty

    def evaluate(self):
        code_analysis_results = self.evaluate_code()
        optimized_query = code_analysis_results["optimized_query"]
        code_stats = code_analysis_results["stats"]
        is_valid = code_analysis_results["valid"] == 1
        self.neighborhood_score = 0
        self.code_style_score = self.calculate_code_style_score(code_stats)
        if is_valid:
            query_filepath = self.compile(optimized_query)
            neighborhood_results = [ r for i in self.instances for r in self.evaluate_neighborhood(query_filepath, i) ]
            if len(neighborhood_results) > 0:
                self.neighborhood_score = sum([self.calculate_neighborhood_score(i) for i in neighborhood_results])/len(neighborhood_results)
            
        if self.clean:
            with Sultan.load(cwd=self.cwd, logging=self.logging) as s:
                s.rm("-rf {}".format(self.tmpdirname)).run() 
        
if __name__ == "__main__":
    evaluator = GcEvaluator(sys.argv[1], ".", logging=True, clean=False)
    evaluator.evaluate()
    print("code-style: {}\nneighborhod: {}".format(evaluator.code_style_score, evaluator.neighborhood_score))