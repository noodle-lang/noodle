#!/usr/bin/env swipl
:- use_module(library(noodle)).
:- consult('domain.ndl').

:- initialization(main, main).

main([Path]) :-
    read(NQL),
    ndl_query(_{serialize:Path, semantics:write, mode:stochastic}, [
            NQL   
        ]).
    