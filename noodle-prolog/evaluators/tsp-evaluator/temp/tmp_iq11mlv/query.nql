%% Head
% Query: temp/tmp_iq11mlv/query
:- use_module(library(noodle)).
:- dynamic warnings_num/1.

warnings_num(0).


%% Body
neighbor(A, D) :-
    ndl_state_init(user, write),
    ndl_state_new(A, B),
    neighbor_(B, C),
    ndl_state_get_solution(C, D).

neighbor_(state{avar:I, cstore:cstore{sat:J, unsat:K}, memory:G, solution:C, stacks:H}, state{avar:B1, cstore:cstore{sat:C1, unsat:D1}, memory:Z, solution:E1, stacks:A1}) :-
    variable(next, A, D),
    variable(next, B, E),
    A<B,
    ndl_get_value(C, D, M),
    ndl_get_value(C, E, F),
    variable(next, F, L),
    iterate_init_1(state{ avar:I,
                          cstore:cstore{sat:J, unsat:K},
                          memory:G,
                          solution:C,
                          stacks:H
                        },
                   state{ avar:P,
                          cstore:cstore{sat:Q, unsat:R},
                          memory:N,
                          solution:S,
                          stacks:O
                        },
                   D,
                   L,
                   F,
                   M,
                   E,
                   B,
                   D,
                   A),
    ndl_set_value_and_propagate(state{ avar:P,
                                       cstore:cstore{ sat:Q,
                                                      unsat:R
                                                    },
                                       memory:N,
                                       solution:S,
                                       stacks:O
                                     },
                                L,
                                M,
                                state{ avar:V,
                                       cstore:cstore{ sat:W,
                                                      unsat:X
                                                    },
                                       memory:T,
                                       solution:Y,
                                       stacks:U
                                     }),
    ndl_set_value_and_propagate(state{ avar:V,
                                       cstore:cstore{ sat:W,
                                                      unsat:X
                                                    },
                                       memory:T,
                                       solution:Y,
                                       stacks:U
                                     },
                                E,
                                A,
                                state{ avar:B1,
                                       cstore:cstore{ sat:C1,
                                                      unsat:D1
                                                    },
                                       memory:Z,
                                       solution:E1,
                                       stacks:A1
                                     }).

neighbor_verbose(A, C) :-
    ndl_state_init(user, write),
    ndl_state_new(A, B),
    neighbor_(B, C).

if_1(state{avar:C, cstore:cstore{sat:D, unsat:E}, memory:A, solution:F, stacks:B}, state{avar:C, cstore:cstore{sat:D, unsat:E}, memory:A, solution:F, stacks:B}, G, H, I, _, _) :-
    G==H,
    !,
    I=H.
if_1(state{avar:C, cstore:cstore{sat:D, unsat:E}, memory:A, solution:F, stacks:B}, state{avar:C, cstore:cstore{sat:D, unsat:E}, memory:A, solution:F, stacks:B}, G, H, M, L, _) :-
    I is G-H,
    variable(order, I, J),
    ndl_avar_get(C, J, K),
    variable(next, K, L),
    ndl_get_value(F, L, N),
    M=N.

iterate_1(state{avar:C, cstore:cstore{sat:D, unsat:E}, memory:A, solution:F, stacks:B}, state{avar:C, cstore:cstore{sat:D, unsat:E}, memory:A, solution:F, stacks:B}, [], _, _, _, _, _, _, _).
iterate_1(state{avar:E, cstore:cstore{sat:F, unsat:G}, memory:C, solution:H, stacks:D}, state{avar:R, cstore:cstore{sat:S, unsat:T}, memory:P, solution:U, stacks:Q}, [variable(next, _, A)-variable(next, I, _)|V], B, W, X, Y, Z, A1, B1) :-
    A\==B,
    ndl_set_value_and_propagate(state{ avar:E,
                                       cstore:cstore{ sat:F,
                                                      unsat:G
                                                    },
                                       memory:C,
                                       solution:H,
                                       stacks:D
                                     },
                                A,
                                I,
                                state{ avar:L,
                                       cstore:cstore{ sat:M,
                                                      unsat:N
                                                    },
                                       memory:J,
                                       solution:O,
                                       stacks:K
                                     }),
    !,
    iterate_1(state{ avar:L,
                     cstore:cstore{sat:M, unsat:N},
                     memory:J,
                     solution:O,
                     stacks:K
                   },
              state{ avar:R,
                     cstore:cstore{sat:S, unsat:T},
                     memory:P,
                     solution:U,
                     stacks:Q
                   },
              V,
              B,
              W,
              X,
              Y,
              Z,
              A1,
              B1).
iterate_1(state{avar:C, cstore:cstore{sat:D, unsat:E}, memory:A, solution:F, stacks:B}, state{avar:C, cstore:cstore{sat:D, unsat:E}, memory:A, solution:F, stacks:B}, [variable(next, _, _)-variable(next, _, _)|_], _, _, _, _, _, _, _).

iterate_init_1(state{avar:D, cstore:cstore{sat:E, unsat:F}, memory:B, solution:G, stacks:C}, state{avar:Q, cstore:cstore{sat:R, unsat:S}, memory:O, solution:T, stacks:P}, A, V, W, X, Y, Z, A, A1) :-
    variable(next, H, A),
    iterate_find_chain_1(state{ avar:D,
                                cstore:cstore{sat:E, unsat:F},
                                memory:B,
                                solution:G,
                                stacks:C
                              },
                         state{ avar:K,
                                cstore:cstore{sat:L, unsat:M},
                                memory:I,
                                solution:N,
                                stacks:J
                              },
                         variable(next, H, A),
                         [variable(next, H, A)],
                         U),
    iterate_1(state{ avar:K,
                     cstore:cstore{sat:L, unsat:M},
                     memory:I,
                     solution:N,
                     stacks:J
                   },
              state{ avar:Q,
                     cstore:cstore{sat:R, unsat:S},
                     memory:O,
                     solution:T,
                     stacks:P
                   },
              U,
              V,
              W,
              X,
              Y,
              Z,
              A,
              A1).

iterate_find_chain_1(state{avar:C, cstore:cstore{sat:D, unsat:E}, memory:A, solution:F, stacks:B}, state{avar:C, cstore:cstore{sat:D, unsat:E}, memory:A, solution:F, stacks:B}, variable(next, H, _), J, []) :-
    variable(next, _, G),
    ndl_get_value(F, G, H),
    variable(next, I, G),
    member(variable(next, I, G), J),
    !.
iterate_find_chain_1(state{avar:H, cstore:cstore{sat:I, unsat:J}, memory:F, solution:D, stacks:G}, state{avar:M, cstore:cstore{sat:N, unsat:O}, memory:K, solution:P, stacks:L}, variable(next, A, B), Q, [variable(next, A, B)-variable(next, E, C)|R]) :-
    variable(next, _, C),
    ndl_get_value(D, C, A),
    variable(next, E, C),
    !,
    iterate_find_chain_1(state{ avar:H,
                                cstore:cstore{sat:I, unsat:J},
                                memory:F,
                                solution:D,
                                stacks:G
                              },
                         state{ avar:M,
                                cstore:cstore{sat:N, unsat:O},
                                memory:K,
                                solution:P,
                                stacks:L
                              },
                         variable(next, E, C),
                         [variable(next, E, C)|Q],
                         R).
iterate_find_chain_1(state{avar:C, cstore:cstore{sat:D, unsat:E}, memory:A, solution:F, stacks:B}, state{avar:C, cstore:cstore{sat:D, unsat:E}, memory:A, solution:F, stacks:B}, variable(next, _, _), _, []).

is_satisfied(diff_self_next, state{avar:_, cstore:cstore{sat:_, unsat:_}, memory:_, solution:A, stacks:_}, B, D) :-
    ndl_get_value(A, B, C),
    C\==D.
is_satisfied(all_diff_next, state{avar:_, cstore:cstore{sat:_, unsat:_}, memory:_, solution:A, stacks:_}, B, C) :-
    ndl_get_value(A, B, D),
    ndl_get_value(A, C, E),
    D\==E.
is_satisfied(all_diff_order, state{avar:A, cstore:cstore{sat:_, unsat:_}, memory:_, solution:_, stacks:_}, B, C) :-
    ndl_avar_get(A, B, D),
    ndl_avar_get(A, C, E),
    D\==E.

get_aux_value(order, state{avar:D, cstore:cstore{sat:E, unsat:F}, memory:B, solution:G, stacks:C}, A, K, J) :-
    constant(minIndex, I),
    variable(order, H, A),
    if_1(state{ avar:D,
                cstore:cstore{sat:E, unsat:F},
                memory:B,
                solution:G,
                stacks:C
              },
         state{ avar:D,
                cstore:cstore{sat:E, unsat:F},
                memory:B,
                solution:G,
                stacks:C
              },
         H,
         I,
         J,
         K,
         A).

