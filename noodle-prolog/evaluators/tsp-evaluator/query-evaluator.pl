#!/usr/bin/env swipl
:- use_module(library(noodle)).
:- use_module(library(yall)).
:- use_module(library(http/json)).
:- use_module(library(occurs)).

:- initialization(main, main).

:- dynamic(constraint_amount/2).
:- dynamic(solution_size/1).
:- dynamic(neighbors_total/2).
:- dynamic(valid_total/2).
:- dynamic(instance_name/1).
:- dynamic(changes_mean/2).
:- dynamic(changes_min/2).
:- dynamic(changes_max/2).
:- dynamic(changes_m2/2).
:- dynamic(satisfied_mean/3).
:- dynamic(satisfied_min/3).
:- dynamic(satisfied_max/3).
:- dynamic(satisfied_m2/3).
:- dynamic(total_duplicates/2).
:- dynamic(initial_cost/2).
:- dynamic(end_cost/2).
:- dynamic(symmetry_total/2).
:- dynamic(unique_costs/2).
:- dynamic(evaluation_time/2).

main([QueryName, DomainPath, TimeLimitString]) :-
    atomic_list_concat([QueryName, '.nql'], QueryPath),
    term_to_atom(TimeLimit, TimeLimitString),
    assert(instance_name(DomainPath)),
    consult(DomainPath),
    consult(QueryPath),
    calculate_instance_stats(),
    MaxSolIndex = 1,
    calculate_scores(MaxSolIndex, TimeLimit),
    create_outputs(MaxSolIndex, Outputs),
    current_output(Stream),
    json_write(Stream, Outputs).

create_outputs(MaxSolIndex, Outputs) :-
    findall(Output, ( 
        between(0, MaxSolIndex, Index),
        create_output(Index, Output)
    ), Outputs).

create_output(Index, output{
        instance: Instance,
        neighbors: Score
    }) :-
        create_instance(Index, Instance),
        create_score(Index, Score).

create_instance(Index, instance{
        constraints: Constraints, 
        name: Name,
        solution: Index,
        variables: Variables
    }) :-
        findall(C-Count, (   
                available_constraint(C),
                constraint_amount(C, Count)
            ),
            ConstraintsList 
        ),
        dict_create(Constraints, constraints, ConstraintsList),
        instance_name(Name),
        solution_size(Variables).
create_score(Index, 
      neighbors{
        amount:Amount,
        duplicates:Duplicates,
        valid:Valid,
        changes: Changes,
        satisfied: Satisfied,
        symmetrical: Symmetry,
        unique_costs: UniqueCosts,
        evaluation_time: EvalTime
      }) :-
    neighbors_total(Index, Amount),
    valid_total(Index, Valid),
    get_stats([changes, Index], Amount, Changes),
    total_duplicates(Duplicates, Index),
    symmetry_total(Symmetry, Index),
    evaluation_time(EvalTime, Index),
    unique_costs(UniqueCosts, Index),
    findall(C-Stats, 
        (   
            available_constraint(C),
            get_stats([satisfied, Index, C], Amount, Stats)
        ),
        SatisfiedList 
    ),
    dict_create(Satisfied, satisfied, SatisfiedList).

calculate_instance_stats() :- 
    forall(available_constraint(C),  (
            aggregate_all(count, constraint(C, _, _), NConstraints),
            assert(constraint_amount(C, NConstraints))
    )),
    findall(V, variable(next,_,V), Vars),
    length(Vars, SolutionSize),
    assert(solution_size(SolutionSize)).

calculate_scores(MaxSolIndex, TimeLimit) :- 
    RealTimeLimit is TimeLimit / (MaxSolIndex + 1),
    forall(between(0, MaxSolIndex, SolutionIndex), (
        solution(InitialSolution, SolutionIndex),
        reset_duplicates(SolutionIndex),
        reset_stats(SolutionIndex),
        reset_costs(InitialSolution, SolutionIndex),
        get_time(StartTime),
        calculate_next_neighbor_score(InitialSolution, SolutionIndex, StartTime, RealTimeLimit)
    )).

calculate_next_neighbor_score(InitialSolution, SolutionIndex, StartTime, TimeLimit) :-
    neighbor_verbose(InitialSolution, EndState),
    increment([neighbors_total, SolutionIndex]),
    calculate_changes(EndState, InitialSolution, SolutionIndex),
    calculate_satisfied(EndState, SolutionIndex),
    calculate_duplicates(InitialSolution, EndState, SolutionIndex),
    calculate_costs(EndState, SolutionIndex),
    get_time(CurrentTime),
    Duration is CurrentTime - StartTime,
    (Duration > TimeLimit ->
        assert(evaluation_time(Duration, SolutionIndex)),
        !
        ;
        fail).
calculate_next_neighbor_score(_, SolutionIndex, StartTime, _) :-
    get_time(CurrentTime),
    Duration is CurrentTime - StartTime,
    assert(evaluation_time(Duration, SolutionIndex)).

calculate_changes(EndState, 
                  InitialSolution,
                  SolutionIndex) :-
    solution_size(SolutionSize),
    ndl_state_get_solution(EndState, EndSolution),
    ndl_diff(InitialSolution, EndSolution, Diff),
    length(Diff, DiffN),
    safe_fdiv(DiffN, SolutionSize, NormalizedDiff),
    IsValid is ceiling(NormalizedDiff),
    update([changes, SolutionIndex], NormalizedDiff, SolutionIndex),
    increment([valid_total, SolutionIndex], IsValid).

calculate_satisfied(EndState, SolutionIndex) :- 
    ndl_state_get_cstore(EndState, CStore),
    forall(available_constraint(C), (
        constraint_amount(C, CCount),
        aggregate_all(count, ndl_cstore_violated(CStore, C, _), TotalError),
        SatisfiedPercentage is 1 - TotalError / CCount,
        update([satisfied, SolutionIndex, C], SatisfiedPercentage, SolutionIndex)
    )).

reset_duplicates(SolutionIndex) :-
    retractall(end_state(_)),
    assert(total_duplicates(0, SolutionIndex)).

calculate_duplicates(InitialSolution, EndState, SolutionIndex) :-
    ndl_state_get_solution(EndState, EndSolution),
    ndl_diff(InitialSolution, EndSolution, Diff),
    ( 
        end_state(Diff) ->
            total_duplicates(TD, SolutionIndex),
            NTD is TD + 1,
            retractall(total_duplicates(TD, SolutionIndex)),
            assert(total_duplicates(NTD, SolutionIndex))
        ;
        assert(end_state(Diff))
    ).

reset_costs(Solution, SolutionIndex) :-
    cost_function(Solution, Cost),
    assert(initial_cost(Cost, SolutionIndex)),
    assert(symmetry_total(0, SolutionIndex)),
    assert(unique_costs(0, SolutionIndex)).

calculate_costs(NeighborState, SolutionIndex) :-
    ndl_state_get_solution(NeighborState, Neighbor),
    initial_cost(InitialCost, SolutionIndex),
    cost_function(Neighbor, Cost),
    (InitialCost = Cost -> 
        symmetry_total(Total, SolutionIndex),
        NewTotal is Total + 1,
        retractall(symmetry_total(Total, SolutionIndex)),
        assert(symmetry_total(NewTotal, SolutionIndex))
        ;
        true
    ),
    (end_cost(Cost, SolutionIndex) ->
        true
        ;
        assert(end_cost(Cost, SolutionIndex)),
        unique_costs(Total, SolutionIndex),
        NewTotal is Total + 1,
        retractall(unique_costs(Total, SolutionIndex)),
        assert(unique_costs(NewTotal, SolutionIndex))
    ).



get_stats([Name | Args], Amount, 
         stats{
             min_val: MinVal, 
             max_val: MaxVal, 
             avg_val: AvgVal,
             std_dev: StdDev}) :-
    atomic_concat(Name, '_mean', MeanName),
    atomic_concat(Name, '_m2', M2Name),
    atomic_concat(Name, '_max', MaxName),
    atomic_concat(Name, '_min', MinName),
    create_term([MeanName|Args], AvgVal, MeanTerm),
    create_term([M2Name|Args], M2Value, M2Term),
    create_term([MaxName|Args], MaxVal, MaxTerm),
    create_term([MinName|Args], MinVal, MinTerm),
    call(MeanTerm),
    call(MaxTerm),
    call(MinTerm),
    call(M2Term),
    (Amount > 2 ->
        StdDev is M2Value / (Amount-1)
        ;
        StdDev is M2Value).
    
reset_stats(SolutionIndex) :-
    number(SolutionIndex),
    reset_stats([changes,SolutionIndex]),
    forall(available_constraint(C), (
        reset_stats([satisfied,SolutionIndex,C])
    )).

reset_stats([Name | Args]) :-
    atomic_concat(Name, '_mean', MeanName),
    atomic_concat(Name, '_m2', M2Name),
    create_term([MeanName | Args], 0, MeanTerm),
    create_term([M2Name | Args], 0, M2Term),
    assert(MeanTerm),
    assert(M2Term).

update([Name | Args], Amount, SolutionIndex) :-
    atomic_concat(Name, '_mean', MeanName),
    atomic_concat(Name, '_m2', M2Name),
    atomic_concat(Name, '_max', MaxName),
    atomic_concat(Name, '_min', MinName),
    create_term([MeanName|Args], MeanValue, MeanTerm),
    call(MeanTerm),
    neighbors_total(SolutionIndex, Count),
    Delta is Amount - MeanValue,
    MeanDelta is Delta/Count,
    Delta2 is Amount - (MeanValue + MeanDelta), 
    M2Delta is Delta * Delta2,
    increment([MeanName | Args], MeanDelta),
    increment([M2Name | Args], M2Delta),
    update_max([MaxName | Args], Amount),
    update_min([MinName | Args], Amount).

increment(Predicate) :-
    increment(Predicate, 1).
increment(Predicate, Amount) :-
    \+ is_list(Predicate), !,
    increment([Predicate], Amount).
increment(Predicate, Amount) :-
    create_term(Predicate, OldAmount, OldTerm),
    (call(OldTerm) ->
        retract(OldTerm),
        NewAmount is OldAmount + Amount,
        create_term(Predicate, NewAmount, NewTerm),
        assert(NewTerm)
        ;
        create_term(Predicate, Amount, NewTerm),
        assert(NewTerm)   
    ).
update_max(Predicate, Value) :-
    \+ is_list(Predicate), !,
    update_max([Predicate], Value).
update_max(Predicate, Value) :-
    create_term(Predicate, OldValue, OldTerm),
    ( call(OldTerm) ->
        NewValue is max(Value, OldValue), 
        retract(OldTerm),
        create_term(Predicate, NewValue, NewTerm),
        assert(NewTerm)
    ;
        create_term(Predicate, Value, NewTerm),
        assert(NewTerm)
    ).
update_min(Predicate, Value) :-
    \+ is_list(Predicate), !,
    update_min([Predicate], Value).
update_min(Predicate, Value) :-
    create_term(Predicate, OldValue, OldTerm),
    ( call(OldTerm) ->
        NewValue is min(Value, OldValue), 
        retract(OldTerm),
        create_term(Predicate, NewValue, NewTerm),
        assert(NewTerm)
    ;
        create_term(Predicate, Value, NewTerm),
        assert(NewTerm)
    ).

create_term(Prefix, Arg, Term) :-
    append(Prefix, [Arg], Args),
    Term =.. Args.

solution(Sol, 1) :- 
    constant_definition(maxIndex, N),
    findall(X, between(1,N,X), Indexes),
    nth0(0,Indexes,First),
    create_circuit(First, Indexes, Circuit),
    ndl_solution(Circuit, Sol).

solution(Sol, 2) :- 
    constant_definition(maxIndex, N),
    findall(X, between(1,N,X), Indexes),
    reverse(Indexes, Reversed),
    nth0(0,Reversed,First),
    create_circuit(First, Reversed, Circuit),
    ndl_solution(Circuit, Sol).

solution(Sol, 0) :- 
    constant_definition(maxIndex, N),
    findall(X, between(1,N,X), Indexes),
    split(Indexes, L, R),
    reverse(L,RL),
    append(RL, R, Swizzled),
    nth0(0,Swizzled,First),
    create_circuit(First, Swizzled, Circuit),
    ndl_solution(Circuit, Sol).

split([], [], []).
split([X], [X], []).
split([X,Y|R], [X|XR], [Y|YR]) :-
    split(R,XR,YR).
    
random_solution(Sol) :-
    constant_definition(maxIndex, N),
    findall(X, between(1,N,X), Indexes),
    random_permutation(Indexes, Permutation),
    nth0(0,Permutation,First),
    create_circuit(First, Permutation, Circuit),
    ndl_solution(Circuit, Sol).

create_circuit(First, [X], [next(X)-First]).
create_circuit(First, [X,Y|Rest], [next(X)-Y|NRest]) :-
    create_circuit(First, [Y|Rest], NRest).

cost_function(Solution, Cost) :-
    findall(Distance, (
        ndl_gen_value(Solution, next(Start), End),
        distance(Start, End, Distance)
    ), Distances),
    sumlist(Distances, Cost).

distance(Start, End, Distance) :-
    K2 is min(Start, End),
    K1 is max(Start, End),
    Distance is ((K1 + K2)*(K1 + K2 + 1))/2 + K2.


    