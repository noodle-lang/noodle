#!/usr/bin/env python3

from typing import Dict, List
from sultan.api import Sultan 
from pathlib import Path
import random
import tempfile 
import sys
import json
import math

class Evaluator:
    def __init__(self, query: str, cwd: str, logging: bool = False, clean: bool = True, time_limit: float = 3):
        self.cwd = cwd
        self.logging = logging
        self.clean = clean
        self.query = query  
        self.instances = ["domain-odd.ndl", "domain-even.ndl"] 
        self.time_limit = time_limit
        self.tmpdirname = "{}/{}".format("temp", Path(tempfile.mkdtemp(dir="{}/temp".format(self.cwd))).stem)


    def compile(self, query: str) -> str:
        with Sultan.load(cwd=self.cwd, logging=self.logging) as s:
            query_filepath = "{}/query".format(self.tmpdirname)
            s.echo('"{}"'.format(query)).pipe().swipl("./query-compiler.pl {}".format(query_filepath)).run()
            return query_filepath
    
    def evaluate_code(self) -> Dict:
        with Sultan.load(cwd=self.cwd, logging=self.logging) as s:
            output = s.swipl("./query-analyzer.pl \"{}\"".format(self.query[0:-1])).run()
            if self.logging:
                print(output)
            return json.loads(str(output))

    def evaluate_neighborhood(self, query_filepath: str, instance: str) -> List[Dict]:
        with Sultan.load(cwd=self.cwd, logging=self.logging) as s:
            output = s.swipl("./query-evaluator.pl {} {} {}".format(query_filepath, instance, self.time_limit)).run()
            if self.logging:
                print(output)
            return json.loads(str(output))

    def calculate_code_style_score(self, instance:Dict) -> float:
        ignored_outputs_score = None 
        missing_inputs_score = None
        self_loops_score = None
        shadowed_score = None
        effective_score = instance["effective"] / instance["risky_terms_max"]
        n_scores = 1
        total_score =  effective_score

        if instance["ignored_outputs_max"] > 0:
            ignored_outputs_score = 1 - instance["ignored_outputs"] / instance["ignored_outputs_max"]
            total_score += ignored_outputs_score
            n_scores += 1
        if instance["missing_inputs_max"] > 0:
            missing_inputs_score = 1 - instance["missing_inputs"] / instance["missing_inputs_max"]
            total_score += missing_inputs_score
            n_scores += 1
        if instance["self_loops_max"] > 0:
            self_loops_score = 1 - instance["self_loops"] / instance["self_loops_max"]
            total_score += self_loops_score
            n_scores += 1
        if instance["shadowed_max"] > 0:
            shadowed_score = 1 - instance["shadowed"] / instance["shadowed_max"]
            total_score += shadowed_score
            n_scores += 1

        result = total_score / n_scores
        if self.logging:
            print("\n\t".join(["code_style_score =",
                   "ignored_outputs_score = {}".format(ignored_outputs_score),
                   "missing_inputs_score = {}".format(missing_inputs_score),
                   "self_loops_score = {}".format(self_loops_score),
                   "shadowed_score = {}".format(shadowed_score),
                   "effective_score = {}".format(effective_score),
                   "/ {}".format(n_scores),
                   "= {}".format(result)]))
        return result

    def calculate_neighborhood_score(self, instance:Dict) -> float:
        def evaluate_sat_score(s:Dict, size:int) -> float:
            if s["min_val"] == 1.0:
                return 1.0
            if s["max_val"] == 1.0:
                return s["avg_val"] / size
            return 0

        sat_square_sum = sum([s["min_val"] * s["min_val"] for s in instance["neighbors"]["satisfied"].values()]) / len(instance["neighbors"]["satisfied"])
        changes = instance["neighbors"]["changes"]["avg_val"]
        size = instance["instance"]["variables"]
        unsat_penalty = (1 - sat_square_sum)/(changes+0.00000001)
        normalized_unsat_penalty = unsat_penalty/size
        sat_score = 1 - normalized_unsat_penalty
        
        sat_constraint_bonus = sum([evaluate_sat_score(s, size) for s in instance["neighbors"]["satisfied"].values()]) / len(instance["neighbors"]["satisfied"])
        
        total_amount = instance["neighbors"]["amount"]
        amount = total_amount - instance["neighbors"]["duplicates"]  
        satisfiable_amount = math.factorial(size) / (2 * math.factorial(size - 2))
        amount_score = 1/(1 + math.exp(0.5 * (-amount + satisfiable_amount/2)))

        changes_stdev = instance["neighbors"]["changes"]["std_dev"]
        changes_score = 1/(1 + math.exp(40 * (-changes_stdev + 0.06)))

        if self.logging:
            print('neighborhood_score = {} + {} + {} * {} * {}'.format(sat_score, sat_constraint_bonus, amount_score, amount / total_amount, changes_score))
        return sat_score + sat_constraint_bonus \
               + amount_score * 0.05 \
               + changes_score * 0.05 \
               + amount_score * (amount / total_amount) * 0.1 \
               + amount_score * changes_score * 0.1 \
               + (amount / total_amount) * changes_score * 0.1 \
               + amount_score * (amount / total_amount) * changes_score * 0.6

    def evaluate(self):
        code_analysis_results = self.evaluate_code()
        optimized_query = code_analysis_results["optimized_query"]
        code_stats = code_analysis_results["stats"]
        is_valid = code_analysis_results["valid"] == 1
        self.neighborhood_score = 0
        self.code_style_score = self.calculate_code_style_score(code_stats)
        if is_valid:
            query_filepath = self.compile(optimized_query)
            neighborhood_results = [ r for i in self.instances for r in self.evaluate_neighborhood(query_filepath, i) ]
            if len(neighborhood_results) > 0:
                self.neighborhood_score = sum([self.calculate_neighborhood_score(i) for i in neighborhood_results])/len(neighborhood_results)
            
        if self.clean:
            with Sultan.load(cwd=self.cwd, logging=self.logging) as s:
                s.rm("-rf {}".format(self.tmpdirname)).run() 
        
if __name__ == "__main__":
    evaluator = Evaluator(sys.argv[1], ".", logging=True, clean=False)
    evaluator.evaluate()
    print("code-style: {}\nneighborhod: {}".format(evaluator.code_style_score, evaluator.neighborhood_score))
