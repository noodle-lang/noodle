#!/usr/bin/env python3

from sultan.api import Sultan
import argparse

def arg_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser("Simple deployment script for swipl package")
    parser.add_argument("--install", "-i", action='store_true', help='install pack automatically') 
    parser.add_argument("--version", "-v", default='0.9', help='pack version, e.g. 0.9')
    return parser

def meta_info(version) -> str:
    meta_info = r"\n".join(["\"name(noodle).",
                "title('Neighborhood Description Language').",
                "version('{}').".format(version),
                "author('Mateusz Ślażyński', 'mateusz.slazynski@agh.edu.pl').\""])
    return meta_info

if __name__ == "__main__":
    parser = arg_parser()
    args = parser.parse_args()
    version = args.version
    install = args.install
    with Sultan.load() as s:
        s.rm("-rf deploy").run()
        s.mkdir("-p deploy/noodle/prolog").run()
        s.echo(meta_info(version)).redirect("./deploy/noodle/pack.pl", append=False, stdout=True).run()
        s.cp("noodle*.pl deploy/noodle/prolog").run()
    
    with Sultan.load(cwd="deploy") as s:
        s.tar("zcvf noodle-{}.tgz noodle".format(version)).run()
        if install:
            s.swipl("-g \"pack_install('noodle-{}.tgz', [interactive(false), upgrade(true)]), halt.\"".format(version)).run()


