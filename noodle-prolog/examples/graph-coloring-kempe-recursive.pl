:- consult('graph-coloring-domain.pl').

run :- create_model, create_query.

create_query :-
    ndl_query(_{serialize:'build/graph-coloring-kempe-recursive', mode:stochastic}, [
        kempe/3 : [var(nodes), range(colors), range(colors)],
        (neighbor :- 
            variable(nodes, _, Node),
            get_value(Node, OldColor),
            variable(nodes, _, Node2),
            get_value(Node2, NewColor),
            NewColor \= OldColor,
            kempe(Node, OldColor, NewColor)),
        (kempe(Node, OldColor, NewColor) :-
            set_value(Node, NewColor),
            for_each(constraint(diff, Node, Neighbor), (
                get_value(Neighbor, NewColor),
                kempe(Neighbor, NewColor, OldColor)    
            ))    
        )   
    ]).