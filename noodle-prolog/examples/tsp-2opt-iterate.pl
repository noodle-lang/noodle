:- consult('tsp-advanced-domain.pl').

run :- create_model(), create_query().

create_query() :-
    ndl_query(_{serialize:'build/tsp-2opt-iterate', semantics:write},(
        [
            (neighbor :-
                    variable(next, I1, X1),
                    variable(next, I2, X2),
                    I1 < I2,
                    value(next, X1, V1),
                    value(next, X2, V2),
                    variable(next, V2, X3),
                    iterate_backward(variable(next, _, MX1) - variable(next, MI2, _), X1, (
                        MX1 \= X3,
                        set_value(MX1, MI2)
                    )),
                    set_value(X3, V1),
                    set_value(X2, I1)
            )
        ]
    )).


