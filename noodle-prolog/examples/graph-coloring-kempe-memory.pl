:- consult('graph-coloring-domain.pl').

run :- create_model, create_query.

create_query :-
    ndl_query(_{serialize:'build/graph-coloring-kempe-memory', mode:stochastic}, [
        (neighbor :- 
            variable(nodes, _, Node),
            get_value(Node, OldColor),
            variable(nodes, _, Node2),
            get_value(Node2, NewColor),
            NewColor \= OldColor,
            push(var(nodes), Node),
            while(pop(var(nodes), N), (
                flip_variable(N, NewColor, OldColor), 
                remember(N),
                for_each(constraint(diff, N, N2), (
                    \+ in_memory(N2),
                    is_violated(diff, N, N2),
                    push(var(nodes), N2)
                )) 
            ))
        )   
    ]).