:- consult('graphs/gc_50_9.pl').
:- use_module(library(clpfd)).
:- use_module(library(noodle)).

graph_edge(N1, N2) :-
    edge(N1, N2).
graph_edge(N1, N2) :-
    edge(N2, N1).

create_model :- 
    nodes_number(NodesNumber),
    N is NodesNumber - 1,
    ndl_model(_{serialize:'build/graph-coloring'}, (
        define_range(indexes, 0..N),
        define_range(colors, 0..N),
        define_variable(nodes, [range(indexes)], range(colors)),
        findall(Node1-Node2, (
            graph_edge(N1,N2), 
            variable(nodes, N1, Node1), 
            variable(nodes, N2, Node2)), 
        Edges),
        define_constraint(diff, var(nodes)-var(nodes), Edges),
        define_constraint_semantics((diff(Var1, Var2) :- get_value(Var1, Val1), get_value(Var2, Val2), Val1 \= Val2))
    )),
    create_solution.

create_solution :- 
    nodes_number(NodesNumber), 
    graph_coloring(NodesNumber, Ns),
    labeling([ff], Ns),
    findall(N-V, (
        variable(nodes, I, N),
        nth0(I, Ns, V)
        ), 
        VarVals
    ),
    ndl_solution(VarVals, Solution),
    tell('build/graph-coloring-solution.pl'),
    format('solution(~w).\n', [Solution]),
    told.

graph_coloring(N, Nodes) :-
    Max is N - 1,
    length(Nodes, N),
    Nodes ins 0..Max,
    colored_nodes(0, N, Nodes).

colored_nodes(I, I, _).
colored_nodes(I, N, Nodes) :-
    I < N, 
    findall(I-I2, graph_edge(I,I2), Edges),
    edges(Nodes, Edges),
    NI is I + 1,
    colored_nodes(NI, N, Nodes).
edges(_, []).
edges(Nodes, [I1-I2|Es]) :-
    nth0(I1, Nodes, N1),
    nth0(I2, Nodes, N2),
    N1 #\= N2,
    edges(Nodes, Es).
    
