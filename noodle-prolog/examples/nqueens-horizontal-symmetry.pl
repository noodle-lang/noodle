:- consult('nqueens-domain.pl').

run :- create_model, create_solution, create_query.

create_query :- ndl_query(_{serialize:'build/nqueens-horizontal-symmetry'}, [
    (neighbor :- 
        constant(last_index, LastIndex),
        for_each(variable(queen, _, Q), (
            get_value(Q, Val1),    
            Val2 is LastIndex - Val1,
            set_value(Q, Val2)
        ))
    )
]).