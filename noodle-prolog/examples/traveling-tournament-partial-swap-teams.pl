:- consult('traveling-tournament-domain.pl').

run :- create_model, create_query.

create_query :- ndl_query(_{serialize:'build/traveling-tournament-swap-teams-partial'}, [
    swap_teams_in_round/3 : [range(teams), range(teams), range(rounds)],
    (
        neighbor :-
            range_element(teams, Team1),
            range_element(teams, Team2),
            Team1 \= Team2,
            range_element(rounds, Round),
            swap_teams_in_round(Team1, Team2, Round)
    ),
    ( 
        swap_teams_in_round(Team1, Team2, Round) :-
            variable(match, Team1, Round, Match1),
            variable(match, Team2, Round, Match2),
            swap_values(Match1, Match2),
            get_value(Match1, Opponent1),
            get_value(Match2, Opponent2),
            for_each(variable(match, _, Round, Match), (
                flip_variable(Match, Team1, Team2)
            )),
            for_each(variable(match, Team1, R, Conflict1), (
                get_value(Conflict1, Opponent1),
                swap_teams_in_round(Team1, Team2, R)
            )),
            for_each(variable(match, Team2, R, Conflict2), (
                get_value(Conflict2, Opponent2),
                swap_teams_in_round(Team1, Team2, R)
            ))
    )    
]).