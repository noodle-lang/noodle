:- consult('graph-coloring-domain.pl').

run :- create_model, create_query.

create_query :-
    ndl_query(_{serialize:'build/graph-coloring-kempe-safe'}, [
        (neighbor :- 
            variable(nodes, _, Node),
            get_value(Node, OldColor),
            variable(nodes, _, Node2),
            get_value(Node2, NewColor),
            NewColor \= OldColor,
            flip_variable(Node, OldColor, NewColor),
            walk_over(constraint(diff, N1, N2), Node, (
                is_violated(diff, N1, N2),
                flip_variable(N2, OldColor, NewColor)
            ))
        )
    ]).
