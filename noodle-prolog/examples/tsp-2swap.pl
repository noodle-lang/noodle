:- consult('tsp-domain.pl').

run :- create_model, create_query.
    
create_query() :-
    ndl_query(_{serialize:'build/tsp-2swap', mode:stochastic},(
        [
            (neighbor :-  
                    constraint(diff, Var1, Var2),
                    swap_values(Var1, Var2))
        ]
    )).

