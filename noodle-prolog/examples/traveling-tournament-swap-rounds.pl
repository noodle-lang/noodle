:- consult('traveling-tournament-domain.pl').

run :- create_model, create_query.

create_query :- 
    ndl_query(_{serialize:'build/traveling-tournament-swap-rounds'}, [
        (neighbor :-
            range_element(rounds, Round1),
            range_element(rounds, Round2),
            Round1 \= Round2,
            for_each(range_element(teams, Team), (
                variable(match, Team, Round1, Match1),
                variable(match, Team, Round2, Match2),
                swap_values(Match1, Match2)       
            ))
        )        
    ]).
