:- consult('graph-coloring-domain.pl').

run :- create_model, create_query.

create_query :-
    ndl_query(_{serialize:'build/graph-coloring-kempe-semantic', semantics:write, mode:stochastic}, [
        (neighbor :- 
            variable(nodes, _, Node),
            get_value(Node, OldColor),
            variable(nodes, _, Node2),
            get_value(Node2, NewColor),
            NewColor \= OldColor,
            flip_variable(Node, OldColor, NewColor),
            remember(Node),
            while(violated(diff, N1, N2), (
                if(\+ in_memory(N1), (
                  flip_variable(N1, OldColor, NewColor),
                  remember(N1)
                ), (
                  flip_variable(N2, OldColor, NewColor),
                  remember(N2)
                ))
            ))
        )
    ]).
