:- use_module(library(noodle)).
:- dynamic(edge/2).
:- dynamic(nodes_number/1).

write_graph(Path, Problem) :-
    open(Path, write, Output),
    write_terms(Output, Problem),
    close(Output).

write_terms(_, []).
write_terms(Output, [H|T]) :-
    format(Output, '~w.~n', [H]),
    write_terms(Output, T).

read_graph(Path, Problem) :-
    open(Path, read, Input),
    read_problem_size(Input, Size),
    read_edges(Input, Size, ReversedProblem),
    reverse(ReversedProblem, Problem),
    close(Input).

string_term(X,Y) :- term_string(Y,X).

read_line(Input, []) :- 
    at_end_of_stream(Input), !.
read_line(Input, Line) :-
    read_line_to_codes(Input, Codes), 
    codes_line(Codes, Line).
codes_line(Codes, []) :-
    length(Codes, L),
    L < 3, !.
codes_line(Codes, Line) :-
    string_codes(String, Codes), 
    split_string(String, " ", "", StringNumbers),
    maplist(string_term, StringNumbers, Line). 
    

read_problem_size(Input, [nodes_number(NodesNumber), edges_number(EdgesNumber)]) :-
    read_line(Input, [NodesNumber, EdgesNumber]).

read_edges(Input, Problem, Problem) :- 
    at_end_of_stream(Input).
read_edges(Input, Problem, [edge(EdgeStart, EdgeEnd) | FullProblem]) :-
    \+ at_end_of_stream(Input),
    read_line(Input, [EdgeStart, EdgeEnd]),
    read_edges(Input, Problem, FullProblem).

output_path(Path, OutputPath) :-
    atom_concat(Path, '.pl', OutputPath).
    
run :- 
    cd('examples/graphs'),
    directory_files('.', InputFiles),
    forall((member(InputPath, InputFiles), exists_file(InputPath)), (
        output_path(InputPath, OutputPath),
        writeln(InputPath),
        read_graph(InputPath, Graph),
        % writeln(OutputPath)
        write_graph(OutputPath, Graph)
    )).
    

    
