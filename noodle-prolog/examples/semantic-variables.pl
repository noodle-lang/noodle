:- use_module(library(noodle)).

run :- create_model, create_query.

create_model :- 
    Size = 4,
    ndl_model(_{serialize:'build/semantic-variables'}, (
        LastIndex is Size - 1,
        DoubleIndex is Size * 2 - 1, 
        define_constant(last_index, LastIndex),
        define_range(rows, 0..const(last_index)),
        define_variable(queen, [range(rows)], range(rows)),
        define_variable(double_queen, [range(rows)], 0..DoubleIndex),
        findall(Q1-Q2, (variable(queen, I, Q1), variable(queen, J, Q2), I < J), Queens),
        define_constraint(diff_col, var(queen)-var(queen), Queens),
        define_constraint(diff_diag, var(queen)-var(queen), Queens),
        define_constraint_semantics((diff_col(Var1,Var2) 
            :- get_value(Var1, Val1), get_value(Var2, Val2), Val1 \= Val2 )),
        define_constraint_semantics((diff_diag(Var1,Var2) 
            :- variable(queen, I1, Var1), 
               variable(queen, I2, Var2), 
               get_value(Var1, Val1), 
               get_value(Var2, Val2),
               SAx is I1 - I2,
               SAy is Val1 - Val2, 
               Ax is abs(SAx),
               Ay is abs(SAy),
               Ax \= Ay 
        )),
        reify_constraint(diff_col),
        reify_constraint(diff_diag),
        findall(D-Q, (range_element(rows, R), variable(queen, R, Q), variable(double_queen, R, D)), DoublePairs),
        make_variable_depend_on(double_queen, var(queen), DoublePairs),
        define_constant(two, 2),
        define_dependency_semantics((double_queen(_, Queen, NewValue) :- get_value(Queen, Val), constant(two, Two), NewValue is Val * Two))
    )).

create_query :- ndl_query(_{serialize:'build/semantic-variables', semantics:write}, [
            (neighbor :- 
                constant(rows_el_1, Index),
                constant(last_index, Value),
                variable(queen, Index, Queen),
                set_value(Queen, Value),
                for_each(variable(diff_col, I, Q), (
                    get_value(Q, V),    
                    format("~w = ~w", [Q, V])
                )),
                for_each(variable(diff_diag, I, Q), (
                    get_value(Q, V),    
                    format("~w = ~w", [Q, V])
                )),
                for_each(variable(double_queen, I, Q), (
                    get_value(Q, V),
                    format("~w = ~w", [Q, V])    
                ))
            )
        ]).