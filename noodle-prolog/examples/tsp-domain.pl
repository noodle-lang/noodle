:- use_module(library(noodle)).
    
create_model() :- 
    ndl_model(_{serialize:'build/tsp'}, (
            define_constant(n, 8),
            define_constant(one,1),
            define_range(cities, const(one)..const(n)),
            define_variable(visit, [range(cities)], range(cities)),
            findall(Var1-Var2, (variable(visit, _, Var1), variable(visit, _, Var2), Var1 \= Var2), VisitPairs),
            define_constraint(diff, var(visit)-var(visit), VisitPairs)
        )).

