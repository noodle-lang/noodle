:- consult('traveling-tournament-domain.pl').

run :- create_model, create_query.

create_query :- ndl_query(_{serialize:'build/traveling-tournament-swap-teams'}, [
    (neighbor :-
        range_element(teams, Team1),
        range_element(teams, Team2),
        Team1 \= Team2,
        for_each(range_element(rounds, Round), (
            variable(match, Team1, Round, Match1),
            variable(match, Team2, Round, Match2),
            get_value(Match1, Opponent1),
            get_value(Match2, Opponent2),
            Opponent2 \= Team1,
            Opponent1 \= Team2, 
            swap_values(Match1, Match2)       
        ))
    )        
]).