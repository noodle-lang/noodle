:- consult('tsp-domain.pl').

run :- create_model(), create_query().

create_query() :-
    ndl_query(_{serialize:'build/tsp-2opt'},(
        [
            reverse_trip/2 : [range(cities), range(cities)],
            (reverse_trip(Start, End) :-
                Start < End),
            (reverse_trip(Start, End) :-
                End > Start,
                variable(visit, Start, City1),
                variable(visit, End, City2),
                constant(one, One),
                swap_values(City1, City2),
                NewStart is Start + One,
                NewEnd is End - One,
                reverse_trip(NewStart, NewEnd)  
            ),
            (neighbor :-
                    range_element(cities, Start),
                    range_element(cities, End),
                    End > Start,
                    reverse_trip(End, Start))
        ]
    )).


