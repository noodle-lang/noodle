:- consult('tsp-advanced-domain.pl').

run :- create_model(), create_query().

create_query() :-
    ndl_query(_{serialize:'build/tsp-2opt-walk-over', semantics:write},(
        [
            (neighbor :-
                    variable(next, I1, X1),
                    variable(next, I2, X2),
                    I1 < I2,
                    format('~w - ~w', [X1, X2]),
                    get_value(X1, V1),
                    get_value(X2, V2),
                    variable(next, V2, X3),
                    set_value(X2, I1),
                    walk_over(constraint(all_diff_next, MX1, MX2), X2, (
                      MX1 \= X3,
                      get_value(MX1, MV1),
                      variable(next, _, MX1),
                      variable(next, MI2, MX2),
                      format('walk_over(all_diff_next, ~w, ~w)', [MX1, MX2]),
                      if(MV1 \= MI2, (
                        is_violated(all_diff_next, MX1, MX2),
                        format('is_violated(all_diff_next, ~w, ~w)', [MX1, MX2]),
                        variable(next, MV1, MX3),
                        format('set_value(~w,~w)', [MX3, MI2]),
                        set_value(MX3, MI2)  
                      ))
                    )),
                    set_value(X3, V1)
            )
        ]
    )).


