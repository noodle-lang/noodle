:- consult('traveling-tournament-domain.pl').

run :- create_model, create_query.

create_query :- ndl_query(_{serialize:'build/traveling-tournament-partial-swap-teams-semantic'}, [
    swap_teams_in_round/3 : [range(teams), range(teams), range(rounds)],
    (
        neighbor :-
            variable(match, Team1, Round, Match1),
            variable(match, Team2, Round, Match2),
            Team1 \= Team2,
            swap_teams_in_round(Team1, Team2, Round),
            walk_over(constraint(rounds_diff, M1, C1), Match1, (
                is_violated(rounds_diff, M1, C1),
                swap_teams_in_round(M1, C2, ConflictRound)
            )),
            walk_over(constraint(rounds_diff, M1, C2), Match2, (
                is_violated(rounds_diff, M1, C2),
                swap_teams_in_round(M1, C2, ConflictRound)
            ))
    ),
    ( 
        swap_teams_in_round(Team1, Team2, Round) :-
            variable(match, Team1, Round, Match1),
            variable(match, Team2, Round, Match2),
            get_value(Match1, Opponent1),
            get_value(Match2, Opponent2),
            for_each(variable(match, _, Round, M), (
                flip_variable(M, Opponent1, Opponent2)    
            )),
            for_each(variable(match, _, Round, M), (
                flip_variable(M, Team1, Team2)    
            ))
    )        
]).