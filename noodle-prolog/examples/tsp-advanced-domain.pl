
:- use_module(library(noodle)).

create_model() :- 
    ndl_model(_{serialize:'build/tsp-advanced'}, (
            define_constant(maxIndex, 6),
            define_constant(minIndex, 1),

            define_range(cities, const(minIndex)..const(maxIndex)),
            define_variable(next, [range(cities)], range(cities)),
            define_variable(order, [range(cities)], range(cities)),
            
            % forall(i in S)(next[i] != i)
            findall(N-I, variable(next, I, N), NextIndexPairs),
            define_constraint(diff_self_next, var(next)-range(cities), NextIndexPairs),
            define_constraint_semantics((diff_self_next(Var,Index) 
                :- get_value(Var, Val), Val \= Index )),
            
            % alldifferent(next)
            findall(N1-N2, (variable(next,_,N1), variable(next,_,N2), N1 \= N2), NextPairs),
            define_constraint(all_diff_next, var(next)-var(next), NextPairs),
            define_constraint_semantics((all_diff_next(Var1,Var2) 
                :- get_value(Var1, Val1), get_value(Var2, Val2), Val1 \= Val2 )),
            
            % alldifferent(order)
            findall(O1-O2, (variable(order,_,O1), variable(order,_,O2), O1 \= O2), OrderPairs),
            define_constraint(all_diff_order, var(order)-var(order), OrderPairs),
            define_constraint_semantics((all_diff_order(Var1,Var2) 
                :- get_value(Var1, Val1), get_value(Var2, Val2), Val1 \= Val2 )),
            

            % if n = 1 then orden[n] = next[] else n > 1: order[n] = next[order[n-1]]
            findall(O-N, (variable(order, _, O), variable(next, _, N)), OrdersToNexts),
            make_variable_depend_on(order, var(next), OrdersToNexts),
            define_dependency_semantics((order(Order, Next, NewValue) 
                :- constant(minIndex, MinIndexConst), 
                   variable(order, Index, Order), 
                   if(Index = MinIndexConst, ( 
                        NewValue is MinIndexConst
                    ), (
                        PrevIndex is Index - MinIndexConst,
                        variable(order, PrevIndex, PrevOrder),
                        get_value(PrevOrder, Prev),
                        variable(next, Prev, Next),
                        get_value(Next, NextValue),
                        NewValue is NextValue
                    ))
                ) 
            )
        )
    ).


