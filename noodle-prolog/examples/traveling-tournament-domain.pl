:- use_module(library(noodle)).

create_model :-
    TeamsNumber = 5,
    RoundsNumber is TeamsNumber - 1,
    ndl_model(_{serialize:'build/traveling-tournament'}, (
        define_range(rounds, 1..RoundsNumber),
        define_range(teams, 1..TeamsNumber),
        define_variable(match, [range(teams), range(rounds)], range(teams)),
        findall(Var1-Var2, (
            variable(match, _, R, Var1),
            variable(match, _, R, Var2),
            Var1 \= Var2
            ),
            RoundVariables    
        ),
        findall(Var1-Var2, (
            variable(match, T, _, Var1),
            variable(match, T, _, Var2),
            Var1 \= Var2
            ),
        TeamVariables),
        findall(Var1-T, variable(match, T, _, Var1), TeamMatches),

        define_constraint(rounds_diff, var(match)-var(match), RoundVariables),
        define_constraint_semantics((
           rounds_diff(M1, M2) :- get_value(M1, V1), get_value(M2, V2), V1 \= V2  
        )),
        define_constraint(team_diff, var(match)-var(match), TeamVariables),
        define_constraint_semantics((
            team_diff(T1, T2) :- get_value(T1, V1), get_value(T2, V2), V1 \= V2  
        )),
        define_constraint(self_match, var(match)-const(teams), TeamMatches),
        define_constraint_semantics((
            self_match(M, T) :- variable(match, T, _, M), get_value(M, V), V \= T
        )),
        define_constraint(opponents, var(match)-var(match), RoundVariables),
        define_constraint_semantics((
            opponents(M1, M2) :- 
                variable(match, T1, _, M1), 
                variable(match, T2, _, M2),
                get_value(M1, V1),
                get_value(M2, V2),
                if(T1 = V2, T2 = V1),
                if(T2 = V1, T1 = V2)
        ))
    )).
