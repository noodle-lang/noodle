:- consult('graph-coloring-domain.pl').

run :- create_model, create_query.

create_query :-
    ndl_query(_{serialize:'build/graph-coloring-kempe-safe-value'}, [
        (neighbor :- 
            value(nodes, Node, OldColor),
            value(nodes, _, NewColor),
            NewColor \= OldColor,
            flip_variable(Node, OldColor, NewColor),
            walk_over(constraint(diff, N1, N2), Node, (
                is_violated(diff, N1, N2),
                flip_variable(N2, OldColor, NewColor)
            ))
        )
    ]).
