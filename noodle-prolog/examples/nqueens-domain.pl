:- use_module(library(noodle)).
:- use_module(library(clpfd)).

size(8).

create_model :- 
    size(Size),
    ndl_model(_{serialize:'build/nqueens'}, (
        LastIndex is Size - 1,
        define_constant(last_index, LastIndex),
        define_range(rows, 0..const(last_index)),
        define_variable(queen, [range(rows)], range(rows)),
        findall(Q1-Q2, (variable(queen, _, Q1), variable(queen, _, Q2), Q1 \= Q2), Queens),
        define_constraint(diff_col, var(queen)-var(queen), Queens),
        define_constraint(diff_diag, var(queen)-var(queen), Queens)
    )),
    create_solution.

create_solution :- 
    size(Size),  
    n_queens(Size, Qs),
    labeling([ff], Qs),
    findall(Q-V, (
        variable(queen, I, Q),
        nth0(I, Qs, V)
        ), 
        VarVals
    ),
    ndl_solution(VarVals, Solution),
    tell('build/nqueens-solution.pl'),
    format('solution(~w).\n', [Solution]),
    told.

n_queens(N, Qs) :-
    Max is N - 1,
    length(Qs, N),
    Qs ins 0..Max,
    safe_queens(Qs).

safe_queens([]).
safe_queens([Q|Qs]) :-
    safe_queens(Qs, Q, 1),
    safe_queens(Qs).

safe_queens([], _, _).
safe_queens([Q|Qs], Q0, D0) :-
    Q0 #\= Q,
    abs(Q0 - Q) #\= D0,
    D1 #= D0 + 1,
    safe_queens(Qs, Q0, D1).