:- consult('traveling-tournament-domain.pl').

run :- create_model, create_query.

create_query :- ndl_query(_{serialize:'build/traveling-tournament-partial-swap-teams-semantic', semantics:write}, [
    swap_teams_in_round/3 : [range(teams), range(teams), range(rounds)],
    (
        neighbor :-
            variable(match, Team1, Round, Match1),
            variable(match, Team2, Round, Match2),
            Team1 \= Team2,
            swap_teams_in_round(Match1, Match2, Round),
            while(violated(rounds_diff, Match1, Conflict1),(
                variable(match, Team1, ConflictRound, Conflict1),
                variable(match, Team2, ConflictRound, Conflict2),
                swap_teams_in_round(Conflict1, Conflict2, ConflictRound)
            )),

            while(violated(rounds_diff, Match2, Conflict2),(
                variable(match, Team2, ConflictRound, Conflict2),
                variable(match, Team1, ConflictRound, Conflict1),
                swap_teams_in_round(Conflict1, Conflict2, ConflictRound)
            ))
    ),
    ( 
        swap_teams_in_round(Match1, Match2, Round) :-
            swap_values(Match1, Match2),
            violated(opponents, Match1, Op1),
            violated(opponents, Match2, Op2),
            swap_values(Op1, Op2)
    )        
]).