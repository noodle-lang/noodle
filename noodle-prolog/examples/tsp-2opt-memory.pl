:- consult('tsp-domain.pl').

run :- create_model(), create_query().

create_query() :-
    ndl_query(_{serialize:'build/tsp-2opt-memory', mode:stochastic},(
        [
            (neighbor :-
                    range_element(cities, Start),
                    range_element(cities, End),
                    Start < End,
                    for_each(range_element(cities, I), (
                        I >= Start, 
                        I =< End,
                        variable(visit, I, Var),
                        get_value(Var, Val),
                        push(range(cities), Val)
                    )),
                    for_each(range_element(cities, I), (
                        I >= Start, 
                        I =< End,
                        variable(visit, I, Var),
                        pop(range(cities), Val),
                        set_value(Var, Val)
                    )))
        ]
    )).


