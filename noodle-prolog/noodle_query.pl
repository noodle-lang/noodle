:- module(noodle_query, [
    ndl_query/1,
    ndl_query/2,
    op(400, yfx, 'in')
]).

:- use_module(noodle).
:- use_module(noodle_typing).
:- use_module(noodle_propagator).
:- reexport([noodle_propagator]).

:- dynamic(internal_predicate/1).
:- dynamic(query_predicate/1).
:- dynamic(constraint_predicate/1).
:- dynamic(aux_predicate/1).
:- dynamic(ndl_debug/1).
:- dynamic(ndl_mode/1).
:- dynamic(ndl_namespace/1).
:- dynamic(ndl_semantics/1).
:- dynamic(ndl_serialize_target/1).
:- dynamic(warnings_num/1).

dynamic_predicate(internal_predicate/1, private).
dynamic_predicate(constraint_predicate/1, private).
dynamic_predicate(aux_predicate/1, private).
dynamic_predicate(query_predicate/1, private).
dynamic_predicate(ndl_debug/1, private).
dynamic_predicate(ndl_namespace/1, private).
dynamic_predicate(ndl_serialize_target/1, private).
dynamic_predicate(ndl_semantics/1, private).


init(Options) :-
    (get_dict(namespace, Options, Namespace) -> true; Namespace = user),
    (get_dict(debug, Options, Debug) -> true; Debug = false),
    (get_dict(mode, Options, Mode) -> true; Mode = exhaustive),
    (get_dict(semantics, Options, Semantics) -> true; Semantics = write),
    (get_dict(serialize, Options, Serialize) -> true; Serialize = []),
    assert(ndl_serialize_target(Serialize)),
    assert(ndl_namespace(Namespace)),
    assert(ndl_debug(Debug)),
    assert(ndl_mode(Mode)),
    assert(ndl_semantics(Semantics)),
    ndl_typing_init(Namespace),
    ndl_state_init(Namespace, Semantics),
    assert(query_predicate(neighbor/0)),
    assert(query_predicate(neighbor_/0)),
    assert(query_predicate(neighbor_verbose/0)),
    assert(warnings_num(0)).

finalize() :-
    ((ndl_serialize_target(Name), Name \= []) -> 
        serialize(Name); 
        compile_predicates()).

serialize(Name) :-
    atom_concat(Name, '.nql', Filename),
    ndl_namespace(Namespace),
    tell(Filename),
    writef('%% Head\n'),
    writef('% Query: %w\n', [Name]), 
    writeln(':- use_module(library(noodle)).'), 
    listing(warnings_num/1),
    (ndl_debug(true) ->
        (
            writef('\n%% Debug \n'),
            forall(dynamic_predicate(Predicate/Arity, private), 
            listing(Predicate/Arity))
        ); true
    ),
    writef('\n%% Body\n'),
    findall(Predicate/Arity, (
        query_predicate(Predicate/A),
        Arity is A + 2
    ), QueryPredicates),
    
    compile_predicates(),
    forall(member(Predicate, QueryPredicates), 
        listing(Namespace:Predicate)
    ),
    (current_predicate(Namespace:is_satisfied/4) -> 
        compile_predicates([Namespace:is_satisfied/4]),
        listing(Namespace:is_satisfied/4); true),
    (current_predicate(Namespace:get_aux_value/5) -> 
        compile_predicates([Namespace:get_aux_value/5]),
        listing(Namespace:get_aux_value/5); true),
    told.

compile_predicates() :-
    ndl_namespace(Namespace),
    findall(Namespace:Predicate/Arity, (
        query_predicate(Predicate/A),
        Arity is A + 2), 
        Predicates
    ),
    compile_predicates(Predicates),
    
    (ndl_debug(false) ->
        (forall(dynamic_predicate(PrivatePredicate/Arity, private),
        (
            compound_name_arity(ToRetract, PrivatePredicate, Arity),
            retractall(ToRetract))
        )
    ); true).    

ndl_query(Query) :-
    ndl_query(_{}, Query).
ndl_query(Options, Program) :-
    init(Options),
    compile_semantics(),
    compile_program(Program),
    finalize().

compile_semantics() :-
    ndl_semantics(read).

compile_semantics() :-
    ndl_mode(Mode),
    retractall(ndl_mode(Mode)),
    assert(ndl_mode(exhaustive)),
    ndl_semantics(write),
    ndl_namespace(N),
    forall(N:constraint_semantics(Name, _), compile_constraint_semantics(Name)),
    forall(N:auxillary_semantics(Name, _), compile_auxillary_semantics(Name)),
    retractall(ndl_mode(exhaustive)),
    assert(ndl_mode(Mode)).

compile_program(Program) :-
    ndl_typing(Context),
    compile_program(Program, Context).
compile_program([], _).
compile_program([(Head :- Body)|Rest], Context) :-
    !,
    ndl_state_empty(StartState),
    compile_head(Context, HeadContext, StartState, EndState, Head, CompiledHead),
    compile_body(HeadContext, BodyContext, StartState, EndState, Body, CompiledBody),
    ndl_namespace(N),
    (ndl_debug(true) ->
        N:assert(CompiledHead :- (CompiledBody, context(BodyContext)))
        ;
        N:assert(CompiledHead :- CompiledBody)),
    compile_program(Rest, Context).
compile_program([Predicate/Arity:Types|Rest], Context) :-
    ndl_typing_remember_predicate(Predicate, Types),
    assert(query_predicate(Predicate/Arity)),
    compile_program(Rest, Context).

compile_head(StartContext, StartContext, StartState, EndState, Head, CompiledHead) :-
    Head =.. [neighbor], !,
    ndl_namespace(N),
    ndl_semantics(Sem),
    ndl_state_get_solution(StartState, SS),
    N:assert((neighbor(SS, ES) :- ndl_state_init(user, Sem), ndl_state_new(SS, InitialState), neighbor_(InitialState, EndState), ndl_state_get_solution(EndState, ES))),
    N:assert((neighbor_verbose(SS, EndState) :- ndl_state_init(user, Sem), ndl_state_new(SS, InitialState), neighbor_(InitialState, EndState))),
    CompiledHead =.. [neighbor_, StartState, EndState].
compile_head(StartContext, StartContext, StartState, EndState, Head, CompiledHead) :-
    Head =.. [Predicate | Args],
    internal_predicate(Predicate), !,
    CompiledHead =.. [Predicate, StartState, EndState | Args]. 
compile_head(StartContext, StartContext, StartState, EndState, Head, CompiledHead) :-
    Head =.. [Predicate], !,
    CompiledHead =.. [Predicate, StartState, EndState]. 
compile_head(StartContext, EndContext, StartState, EndState, Head, CompiledHead) :-
    Head =.. [Predicate | Args],
    ndl_typing_add_predicate(Predicate, Args, StartContext, EndContext),
    CompiledHead =.. [Predicate, StartState, EndState | Args].

compile_body(StartContext, FinishContext, StartState, FinishState, Body, CompiledBody) :-
    conjuct_to_list(Body, QueryList),
    compile_query(StartContext, FinishContext,
        StartState, FinishState, QueryList, BodyList),
    list_to_conjuct(BodyList, CompiledBody).

compile_query(Context, Context, Finish, Finish, [], []).
compile_query(Context, CompleteContext, CurrentState, Finish, [Term|Query], [NewTerm|Body]) :-
    compile_term(Context, NewContext, CurrentState, NewState, Term, NewTerm),
    compile_query(NewContext, CompleteContext, NewState, Finish, Query, Body).

compile_term(OldContext, NewContext, OldState, NewState, Term, CompiledTerm) :-
    ndl_state_empty(NewState),
    query_term(OldContext, NewContext, OldState, NewState, Term, CompiledTerm), !.
compile_term(_, _, _, _, Term, _) :-
    writef('Compile error - unknown term: %w.\n', [Term]), fail.

query_term(OC, NC, OS, NS, Term, CompiledTerm) :-
    once(p_query_term(OC, NC, OS, NS, Term, CompiledTerm)).
p_query_term(OC, NC, O, O, get_value(A1,A2), CT) :-
    ndl_typing_of(A1, OC, A1_Type),
    ndl_typing_variable_name(A1_Type, VarType, VarName),
    compile_get_value(VarName : VarType, O, A1, A2, CT),
    ndl_typing_of(domain(VarName), A2_Type),
    ndl_typing_add(A2, A2_Type, OC, NC).
p_query_term(OC, OC, O, O, get_value(A1,_), true) :- 
    (is_bound(A1,OC) -> true ; increment_warnings(0.5)).

p_query_term(OC, NC, O, O, value(_,A1,A2), CT) :-
    is_bound(A1, OC), !,
    p_query_term(OC, NC, O, O, get_value(A1,A2), CT).
p_query_term(OC, NC, O, O, value(Name,A1,A2), CT) :-
    is_bound(A2, OC), !,
    ndl_namespace(N),
    N:available_variable(Name/_),
    ndl_typing_variable_subtype(var(Name), A1_Type),
    ndl_typing_add(A1, A1_Type, OC, NC),
    ndl_typing_variable_name(A1_Type, VarType, VarName),
    compile_gen_value(VarName : VarType, O, A1, A2, CT).
p_query_term(OC, NC, O, O, value(Name,A1,A2), CT) :-
    \+ is_bound(A1, OC), \+ is_bound(A2, OC),
    ndl_namespace(N),
    N:available_variable(Name/Indexing),
    append(Indexes, [A1], VarArgs),
    length(Indexes, Indexing),
    GetVar =.. [variable, Name | VarArgs],
    p_query_term(OC, TC, O, O, GetVar, CGetVar),
    GetVal =.. [get_value, A1, A2],
    p_query_term(TC, NC, O, O, GetVal, CGetVal),
    CT = (CGetVar, CGetVal).

p_query_term(OC, OC, O, N, set_value(A1,A2), ndl_set_value(S, A1, A2, NS)) :-
    ndl_semantics(read),
    ndl_state_get_solution(O, S),
    ndl_typing_of(A1, OC, var(_)),
    is_bound(A2, OC),
    ndl_state_set_solution(O, NS, N).
p_query_term(OC, OC, O, N, set_value(A1,A2), ndl_set_value_and_propagate(O, A1, A2, N)) :-
    ndl_semantics(write),
    ndl_typing_of(A1, OC, var(_)),
    is_bound(A2, OC).
p_query_term(OC, OC, O, O, set_value(A1,A2), true) :- 
    (is_bound(A1,OC) -> true ; increment_warnings(0.5)),
    (is_bound(A2,OC) -> true ; increment_warnings(0.5)).

p_query_term(OC, OC, O, N, swap_values(A1,A2), ndl_swap_values(S, A1, A2, NS)) :-
    ndl_semantics(read),
    are_bound([A1,A2], OC),
    ndl_state_get_solution(O, S),
    ndl_state_set_solution(O, NS, N). 
p_query_term(OC, OC, O, N, swap_values(A1,A2), ndl_swap_values_and_propagate(O, A1, A2, N)) :-
    are_bound([A1,A2], OC),
    ndl_semantics(write).
p_query_term(OC, OC, O, O, swap_values(A1,A2), true) :- 
    (is_bound(A1,OC) -> true ; increment_warnings(0.5)),
    (is_bound(A2,OC) -> true ; increment_warnings(0.5)).

p_query_term(OC, OC, O, N, flip_variable(A1,A2,A3), ndl_flip_variable(S,A1,A2,A3,NS)) :-
    ndl_semantics(read),
    are_bound([A1,A2,A3], OC),
    ndl_state_get_solution(O, S),
    ndl_state_set_solution(O, NS, N).
p_query_term(OC, OC, O, N, flip_variable(A1,A2,A3), ndl_flip_variable_and_propagate(O,A1,A2,A3,N)) :-
    are_bound([A1,A2,A3], OC),
    ndl_semantics(write).
p_query_term(OC, OC, O, O, flip_variable(A1,A2,A3), true) :- 
    (is_bound(A1,OC) -> true ; increment_warnings(0.3)),
    (is_bound(A2,OC) -> true ; increment_warnings(0.35)),
    (is_bound(A3,OC) -> true ; increment_warnings(0.35)).

p_query_term(OC, OC, O, N, if(Condition,Then,Else), CompiledTerm) :-
    gensym(if_, ConditionName),
    ndl_typing_variables(OC, Variables),
    Head =.. [ConditionName | Variables],
    ThenBody = (Condition, !, Then),
    ConditionRules = [
        (Head :- ThenBody),
        (Head :- Else)
    ],
    length(Variables, Arity),
    assert(internal_predicate(ConditionName)),
    assert(query_predicate(ConditionName/Arity)),
    compile_program(ConditionRules, OC),
    CompiledTerm =.. [ConditionName, O, N | Variables].
p_query_term(OC, OC, O, N, if(Condition,Then), CompiledTerm) :-
    p_query_term(OC, OC, O, N, if(Condition,Then,true), CompiledTerm).
p_query_term(OC, OC, O, N, for_each(Generator, Query), CompiledTerm) :-
    gensym(for_each_, LoopName),
    ndl_typing_variables(OC, Variables),
    LoopEndHead =.. [LoopName, [] | Variables],
    LoopHead =.. [LoopName, [Generator |  GeneratorsTail] | Variables],
    LoopRecursion =.. [LoopName, GeneratorsTail | Variables],
    LoopRules = [
        (LoopEndHead :- !),
        (LoopHead :- Query, !, LoopRecursion),
        (LoopHead :- LoopRecursion)
    ],
    length(Variables, VarArity),
    Arity is VarArity + 1,
    assert(internal_predicate(LoopName)),
    assert(query_predicate(LoopName/Arity)),
    p_query_term(OC, NC, O, _, Generator, _),
    compile_program(LoopRules, NC),
    generator(OC, O, Generator, CompiledGenerator),
    FindAll =.. [findall, Generator, CompiledGenerator, Results], 
    FirstCall =.. [LoopName, O, N, Results | Variables],
    CompiledTerm = (FindAll, FirstCall).
p_query_term(OC, OC, O, O, for_each(_, Query), true) :-
    conjuct_to_list(Query, ListQuery),
    length(ListQuery, Warnings),
    EstimatedWarnings is Warnings * 0.5 + 1, 
    increment_warnings(EstimatedWarnings).
p_query_term(OC, OC, O, N, iterate(variable(Name, I, V)-variable(Name, I2, V2), Start, Query), CompiledTerm) :-
    p_query_term(OC, OC, O, N, iterate(variable(Name, I, V) - variable(Name, I2, V2), Start, Query, forward), CompiledTerm).
p_query_term(OC, OC, O, N, iterate_backward(variable(Name, I, V) - variable(Name, I2, V2), Start, Query), CompiledTerm) :-
    p_query_term(OC, OC, O, N, iterate(variable(Name, I, V) - variable(Name, I2, V2), Start, Query, backward), CompiledTerm).
p_query_term(OC, OC, O, N, iterate(variable(Name, I1, V1) - variable(Name, I2, V2), Start, Query, Direction), CompiledTerm) :-
    is_bound(Start, OC),
    \+ is_bound(I1, OC),
    \+ is_bound(I2, OC),
    \+ is_bound(V1, OC),
    \+ is_bound(V2, OC),
    gensym(iterate_, IterateName),
    gensym(iterate_init_, IterateInitName),
    gensym(iterate_find_chain_, IterateFindName),
    ndl_typing_variables(OC, Variables),
    
    (Direction = forward ->
        NextCall = ( value(Name, PV1, PI2), variable(Name, PI2, PV2) )
        ;
        NextCall = ( value(Name, PV2, PI1), variable(Name, PI2, PV2) )
    ),
    IterateFindHeadEnd =.. [IterateFindName, variable(Name, PI1, PV1), Visited, []],
    IterateFindBodyLoop = (NextCall, member(variable(Name, PI2, PV2), Visited), !),
    IterateFindHeadNormal =.. [IterateFindName, variable(Name, PI1, PV1), Visited, [variable(Name, PI1, PV1)-variable(Name, PI2, PV2) | Found]],
    IterateFindCallNormal =.. [IterateFindName, variable(Name, PI2, PV2), [variable(Name, PI2, PV2)|Visited], Found],
    IterateFindBodyNormal = (NextCall, !, IterateFindCallNormal),
    
    IterateEndHead =.. [IterateName, [] | Variables],
    IterateNormalHead =.. [IterateName, [variable(Name, I1, V1)-variable(Name, I2, V2) | Rest] | Variables],
    IterateNormalCall =.. [IterateName, Rest | Variables],
    IterateNormalBody = (Query, !, IterateNormalCall),
    
    IterateInitHead =.. [IterateInitName, Start | Variables],
    IterateFindFirstCall =.. [IterateFindName, variable(Name, StartI, Start), [variable(Name, StartI, Start)], List],
    IterateFirstCall =.. [IterateName, List | Variables],
    IterateInitBody = (
        variable(Name, StartI, Start),
        IterateFindFirstCall,
        IterateFirstCall
    ),

    CompiledTerm =.. [IterateInitName, O, N, Start | Variables],

    IterateRules = [
        (IterateFindHeadEnd :- IterateFindBodyLoop),
        (IterateFindHeadNormal :- IterateFindBodyNormal),
        (IterateFindHeadEnd :- true),
        (IterateEndHead :- true),
        (IterateNormalHead :- IterateNormalBody),
        (IterateNormalHead :- true),
        (IterateInitHead :- IterateInitBody)
    ],

    length(Variables, VarArity),
    Arity is VarArity + 1,
    assert(internal_predicate(IterateName)),
    assert(query_predicate(IterateName/Arity)),
    assert(internal_predicate(IterateInitName)),
    assert(query_predicate(IterateInitName/Arity)),
    assert(internal_predicate(IterateFindName)),
    assert(query_predicate(IterateFindName/3)),
    p_query_term(OC, TC, O, _, variable(Name, I1, V1), _),
    p_query_term(TC, NC, O, _, variable(Name, I2, V2), _),
    compile_program(IterateRules, NC).

p_query_term(OC, OC, O, N, walk_over(constraint(Name, V1, V2), Start, Query), CompiledTerm) :-
    p_query_term(OC, OC, O, N, walk_over(constraint(Name, V1, V2), Start, normal, Query), CompiledTerm).
p_query_term(OC, OC, O, N, walk_over_inversed(constraint(Name, V1, V2), Start, Query), CompiledTerm) :-
    p_query_term(OC, OC, O, N, walk_over(constraint(Name, V1, V2), Start, inversed, Query), CompiledTerm).
p_query_term(OC, OC, O, N, walk_over(constraint(Name, V1, V2), Start, Mode, Query), CompiledTerm) :-
    gensym(walk_over_init_, WalkInitName),
    gensym(walk_over_, WalkName),
    are_different([V1,V2,Start]),
    is_bound(Start, OC),
    \+ is_bound(V1, OC),
    \+ is_bound(V2, OC),
    ndl_typing_variables(OC, Variables),
    (Mode = normal -> Generator = constraint(Name, V2, _); Generator = constraint(Name, _, V2)),
    FindNeighbors =.. [findall, Generator, Generator, Ns, NsTail],
    FirstCall =.. [WalkName, Ns-NsTail, [] | Variables],
    WalkInitHead =.. [WalkInitName, Name, V2 | Variables],
    WalkInitBody = (
        FindNeighbors,
        FirstCall
    ),
    WalkEndHead =.. [WalkName, [] - [], _ | Variables],
    WalkHead =.. [WalkName, [constraint(Name, V1, V2) |  Rest] - Tail, History | Variables],
    WalkLoopCall =.. [WalkName, Rest-Tail, History | Variables],
    WalkLoopBody = (
        member(V1-V2, History), !, 
        WalkLoopCall
    ),
    FindAppendNeighbors =.. [findall, Generator, Generator, Tail, NsTail],
    WalkNormalCall =.. [WalkName, Rest - NsTail, [V1-V2 | History] | Variables],
    WalkNormalBody = (
        Query, !,
        FindAppendNeighbors,
        WalkNormalCall
    ), 
    WalkRules = [
        (WalkInitHead :- WalkInitBody),
        (WalkEndHead :- !),
        (WalkHead :- WalkLoopBody),
        (WalkHead :- WalkNormalBody),
        (WalkHead :- WalkLoopCall)
    ],
    length(Variables, VarArity),
    Arity is VarArity + 2,
    assert(internal_predicate(WalkInitName)),
    assert(internal_predicate(WalkName)),
    assert(query_predicate(WalkInitName/Arity)),
    assert(query_predicate(WalkName/Arity)),
    p_query_term(OC, NC, O, _, constraint(Name, V1, V2), _),
    compile_program(WalkRules, NC),
    CompiledTerm =.. [WalkInitName, O, N, Name, Start | Variables].
p_query_term(OC, OC, O, O, walk_over(constraint(V1, V2, Start), _, _, Query), true) :-
    conjuct_to_list(Query, ListQuery),
    length(ListQuery, Warnings),
    EstimatedWarnings is Warnings * 0.5,
    increment_warnings(EstimatedWarnings),
    (\+ is_bound(Start, OC) -> increment_warnings(0.4) ; true),
    (is_bound(V1, OC) -> increment_warnings(0.3) ; true),
    (is_bound(V2, OC) -> increment_warnings(0.3) ; true).

p_query_term(OC, OC, O, N, Term, CompiledTerm) :- 
    Term =.. [Predicate | Args],
    internal_predicate(Predicate),
    CompiledTerm =.. [Predicate, O, N | Args].
p_query_term(OC, NC, O, N, Term, CompiledTerm) :- 
    Term =.. [Predicate | Args],
    length(Args, Arity),
    \+ internal_predicate(Predicate),
    query_predicate(Predicate/Arity),
    ndl_typing_add_predicate(Predicate, Args, OC, NC),
    CompiledTerm =.. [Predicate, O, N | Args].
p_query_term(OC, NC, O, O, Z is Expr, CompiledExpression) :-
    (is_bound(Expr, OC) ->
        CompiledExpression =.. [=, Z, Expr],
        ndl_typing_of(Expr, OC, Type),
        ndl_typing_add(Z, Type, OC, NC)
    ; 
        Expr =.. [_ | Args],
        are_bound(Args, OC),
        are_different([Z | Args]),
        compile_arithmetic_operator(Z is Expr, CompiledExpression),
        ndl_typing_promote(Args, OC, Type),
        ndl_typing_add(Z, Type, OC, NC)
    ).
p_query_term(OC, OC, O, O, _ is _, true) :-
    increment_warnings().

p_query_term(OC, OC, O, O, Comparison, CompiledComparison) :-
    Comparison =.. [Op | Args],
    compile_test_operator(Op, CoOp),
    are_bound(Args, OC),
    CompiledComparison =.. [CoOp | Args].
p_query_term(OC, OC, O, O, Comparison, true) :-
    Comparison =.. [Op | [A1, A2]],
    compile_test_operator(Op, _),
    (is_bound(A1,OC) -> true ; increment_warnings(0.5)),
    (is_bound(A2,OC) -> true ; increment_warnings(0.5)).

p_query_term(OC, OC, O, O, !, !).
p_query_term(OC, OC, O, O, true, true).
p_query_term(OC, OC, O, O, format(X,Y), (format(X,Y), nl)).
p_query_term(OC, OC, O, O, write(X), writeln(X)).
p_query_term(OC, OC, O, O, findall(A,B,C,D), findall(A,B,C,D)).
p_query_term(OC, OC, O, O, member(A,B), member(A,B)).

p_query_term(OC, NC, OS, OS, constant(Name, Var), constant(Name, Var)) :-
    ndl_typing_add(Var, const(Name), OC, NC).
p_query_term(OC, NC, OS, OS, range_element(Name, Var), CompiledTerm) :-
    selector(range_element, Selector),
    CompiledTerm =.. [Selector, Name, Var],
    ndl_typing_add(Var, range(Name), OC, NC).
p_query_term(OC, NC, OS, OS, constraint(Name, Var1, Var2), CompiledTerm) :-
    are_different([Var1, Var2]),
    !,
    ndl_typing_of(arg(Name, 1), Type1),
    ndl_typing_of(arg(Name, 2), Type2),
    ndl_typing_variable_subtype(Type1, MType1),
    ndl_typing_variable_subtype(Type2, MType2),
    ndl_typing_add(Var1, MType1, OC, TC),
    ndl_typing_add(Var2, MType2, TC, NC),
    selector(constraint, Selector),
    CompiledTerm =.. [Selector, Name, Var1, Var2].
p_query_term(OC, OC, OS, OS, constraint(_, _, _), true).
p_query_term(OC, OC, OS, OS, \+ constraint(Name, Var1, Var2), \+ constraint(Name, Var1, Var2)) :-
    ((\+ ndl_typing_of(Var1, OC, _), \+ ndl_typing_of(Var2, OC, _)) ->
        ( writef("Compile error: cannot use negation with no bound variables.\n"), fail ) ; true).

p_query_term(OC, NC, OS, OS, T, CT) :-
    T =.. [variable, Name | Args],
    ndl_namespace(N),
    N:available_variable(Name/Indexing),
    length(Args, Arity),
    Arity is Indexing + 1,
    ndl_typing_add_variable(Name, Args, OC, NC),
    selector(variable, Selector),
    CT =.. [Selector, Name | Args].

p_query_term(OC, OC, OS, NS, push(Type, Var), CT) :-
    ndl_typing_of(Var, OC, Type),
    CT = (
        ndl_state_get_stack(OS, Type, Stack, TS), 
        ndl_stack_push(Stack, Var, NewStack),
        ndl_state_set_stack(TS, Type, NewStack, NS)
    ). 
p_query_term(OC, NC, OS, NS, pop(Type,Var), CT) :-
    CT = (
        ndl_state_get_stack(OS, Type, Stack, TS),
        ndl_stack_pop(Stack, Var, NewStack),
        ndl_state_set_stack(TS, Type, NewStack, NS)
    ),
    ndl_typing_add(Var, Type, OC, NC).
p_query_term(OC, NC, OS, NS, top(Type,Var), CT) :-
    CT = (
        ndl_stack_top(Stack, Var),
        ndl_state_get_stack(OS, Type, Stack, NS)
    ),
    ndl_typing_add(Var, Type, OC, NC).
p_query_term(OC, OC, OS, NS, while(pop(Type,Var), Query), CT) :-
    gensym(while_, LoopName),
    ndl_typing_variables(OC, Variables),
    LoopHead =.. [LoopName | Variables],
    LoopInsideHead =.. [LoopName, Var | Variables],
    LoopRules = [
        (LoopHead :- pop(Type,Var), !, LoopInsideHead),
        (LoopHead :- true),
        (LoopInsideHead :- Query, !, LoopHead),
        (LoopInsideHead :- LoopHead)
    ],
    length(Variables, Arity),
    NArity is Arity + 1,
    assert(internal_predicate(LoopName)),
    assert(query_predicate(LoopName/Arity)),
    assert(query_predicate(LoopName/NArity)),
    compile_program(LoopRules, OC),
    CT =.. [LoopName, OS, NS | Variables].

p_query_term(OC, OC, OS, OS, in_memory(Var), CT) :-
    CT = (
        ndl_state_get_memory(OS, Memory),
        ndl_memory_contains(Memory, Var)
    ).
p_query_term(OC, OC, OS, OS, \+ in_memory(Var), CT) :-
    CT = (
        ndl_state_get_memory(OS, Memory),
        \+ ndl_memory_contains(Memory, Var)
    ).
p_query_term(OC, OC, OS, NS, remember(Var), CT) :-
    CT = (
            ndl_state_get_memory(OS, Memory), 
            ndl_memory_remember(Memory, Var, NewMemory),
            ndl_state_set_memory(OS, NewMemory, NS)
        ). 

p_query_term(OC, OC, OS, OS, is_satisfied(Name, Var1, Var2), is_satisfied(Name, OS, Var1, Var2)) :-
    ndl_semantics(read),
    are_bound([Var1, Var2], OC),
    are_different([Var1, Var2]),
    compile_constraint_semantics(Name).
p_query_term(OC, OC, OS, OS, is_satisfied(Name, Var1, Var2), ndl_cstore_get(CStore, constraint(Name, Var1, Var2), 1)) :-
    ndl_semantics(write),
    are_different([Var1, Var2]),
    are_bound([Var1, Var2], OC),
    ndl_state_get_cstore(OS, CStore).
p_query_term(OC, OC, O, O, is_satisfied(_,_,_), true) :- 
    increment_warnings().

p_query_term(OC, OC, OS, OS, is_violated(Name, Var1, Var2), true) :-
    once(p_query_term(OC, OC, OS, OS, is_satisfied(Name, Var1, Var2), Result)),
    Result = true.
p_query_term(OC, OC, OS, OS, is_violated(Name, Var1, Var2), \+ CT) :-
    p_query_term(OC, OC, OS, OS, is_satisfied(Name, Var1, Var2), CT).

p_query_term(OC, NC, OS, OS, violated(Name, Var1, Var2), CT) :-
    ndl_semantics(write),
    ndl_state_get_cstore(OS, CStore),
    CT = (ndl_state_get_cstore(OS, CStore),
          ndl_cstore_violated(CStore, Name, constraint(Name, Var1, Var2))),
    ndl_typing_of(arg(Name, 1), Type1),
    ndl_typing_of(arg(Name, 2), Type2),
    ndl_typing_add(Var1, Type1, OC, TC),
    ndl_typing_add(Var2, Type2, TC, NC). 
p_query_term(_, _, _, _, violated(_, _, _), _) :- 
    ndl_semantics(read),
    writeln("violated/3 is not supported in the on-read semantics mode"),
    fail.
p_query_term(OC, OC, OS, NS, while(violated(Name, Arg1, Arg2), Query), CT) :-
    ndl_semantics(write),
    gensym(while_, LoopName),
    ndl_typing_variables(OC, Variables),
    LoopHead =.. [LoopName | Variables],
    LoopRules = [
        (LoopHead :- violated(Name, Arg1, Arg2), !, Query, LoopHead),
        (LoopHead :- true)
    ],
    length(Variables, Arity),
    assert(internal_predicate(LoopName)),
    assert(query_predicate(LoopName/Arity)),
    compile_program(LoopRules, OC),
    CT =.. [LoopName, OS, NS | Variables].
p_query_term(_, _, _, _, while(violated(_, _, _),_), _) :- 
    ndl_semantics(read),
    writeln("while(violated/3) is not supported in the on-read semantics mode"),
    fail.

compile_arithmetic_operator(Z is X / Y, safe_div(X,Y,Z)) :- !.
compile_arithmetic_operator(Z is Expr, Z is Expr) :-
    Expr =.. [Op | _],
    member(Op, [+,-,*,mod,min,max,mod,abs]).

compile_test_operator(=, ==).
compile_test_operator(\=, \==).
compile_test_operator(<, <).
compile_test_operator(=<, =<).
compile_test_operator(>, >).
compile_test_operator(>=, >=).

compile_gen_value(Name : Type, O, Var, Val, CT) :-
    member(Type, [var, fvar]),
    ndl_state_get_solution(O, S),
    ndl_namespace(N),
    N:available_variable(Name/Indexing),
    functor(Var, Name, Indexing),
    CT = (
        functor(Var, Name, Indexing),
        ndl_gen_value(S, Var, Val)    
    ).
compile_gen_value(Name : avar, O, Var, Val, CT) :-
    ndl_state_get_avar(O, A),
    ndl_namespace(N),
    N:available_variable(Name/Indexing),
    functor(Var, Name, Indexing),
    CT = (
        functor(Var, Name, Indexing),
        ndl_avar_gen(A, Var, Val)    
    ).
compile_gen_value(Name : rvar, O, Var, Val, CT) :-
    ndl_semantics(write),
    ndl_state_get_cstore(O, CStore),
    CT = (reified_constraint(Name, Arg1-Arg2, Var),
          ndl_cstore_gen(CStore, constraint(Name, Arg1, Arg2), Val)).
compile_gen_value(_ : rvar, _, _, _, fail) :-
    ndl_semantics(read),
    writeln('Geting reified variabled by value is not available in a "read" mode').       

compile_get_value(_ : var, O, Var, Val, ndl_get_value(S, Var, Val)) :-
    ndl_state_get_solution(O, S).
compile_get_value(_ : fvar, O, Var, Val, ndl_get_value(S, Var, Val)) :- 
    ndl_state_get_solution(O, S).
compile_get_value(Name : rvar, O, Var, Val, CT) :-
    ndl_semantics(read),
    compile_constraint_semantics(Name),
    Call =.. [is_satisfied, Name, O, Arg1, Arg2],
    CT = (reified_constraint(Name, Arg1-Arg2, Var), (Call -> Val = 1; Val = 0)).
compile_get_value(Name : rvar, O, Var, Val, CT) :-
    ndl_semantics(write),
    ndl_state_get_cstore(O, CStore),
    CT = (reified_constraint(Name, Arg1-Arg2, Var),
          ndl_cstore_get(CStore, constraint(Name, Arg1, Arg2), Val)).
compile_get_value(Name : avar, O, Var, Val, Term) :-
    ndl_semantics(read),
    aux_predicate(Name), !,
    Call =.. [get_aux_value, Name, O, Var, Arg, Val],
    Term = (
        depends(Name, Var, Arg),  
        Call
    ).
compile_get_value(Name : avar, O, Var, Val, Term) :-
    ndl_semantics(read),
    compile_auxillary_semantics(Name),
    compile_get_value(Name : avar, O, Var, Val, Term).
compile_get_value(_ : avar, O, Var, Val, Term) :-
    ndl_semantics(write),
    ndl_state_get_avar(O, AVar),
    Term = ndl_avar_get(AVar, Var, Val).

compile_auxillary_semantics(Name) :- 
    ndl_namespace(N),
    N:auxillary(Name, DepType),
    N:auxillary_semantics(Name, (Head :- Body)),
    assert(aux_predicate(Name)), 
    ndl_typing(Typing),
    ndl_typing_of(domain(Name), ValType),
    Head =.. [Name, Variable, Dependency, Value],
    CompiledHead =.. [get_aux_value, Name, State, Variable, Dependency, Value],
    ndl_typing_add(Variable, avar(Name), Typing, Typing1),
    ndl_typing_add(Dependency, DepType, Typing1, Typing2),
    ndl_typing_add(Value, ValType, Typing2, Typing3),
    compile_body(Typing3, _, State, State, Body, CompiledBody),
    N:assert((CompiledHead :- CompiledBody)).

compile_constraint_semantics(Name) :- 
    constraint_predicate(Name), !.
compile_constraint_semantics(Name) :- 
    ndl_namespace(N),
    N:constraint_semantics(Name, (Head :- Body)),
    assert(constraint_predicate(Name)),
    ndl_typing(Typing),
    ndl_typing_of(arg(Name, 1), Type1),
    ndl_typing_of(arg(Name, 2), Type2),
    Head =.. [Name, Var1, Var2],
    CompiledHead =.. [is_satisfied, Name, State, Var1, Var2],
    ndl_typing_add(Var1, Type1, Typing, Typing1),
    ndl_typing_add(Var2, Type2, Typing1, Typing2),
    compile_body(Typing2, _, State, State, Body, CompiledBody),
    N:assert((CompiledHead :- CompiledBody)).
    
valid_condition(constraint(_,_,_)).
valid_condition(\+ constraint(_,_,_)).
valid_condition(X) :-
    X =.. [Op | _],
    compile_test_operator(Op, _).

selector(constraint, constraint) :-
    ndl_mode(exhaustive), !.
selector(constraint, random_constraint).
selector(variable, variable) :-
    ndl_mode(exhaustive), !.
selector(variable, random_variable).
selector(range_element, range_element) :-
    ndl_mode(exhaustive), !.
selector(range_element, random_range_element).

generator(Typing, _, G, R) :-
    G =.. [constraint | Args],
    \+ are_bound(Args, Typing),
    R =.. [constraint | Args].

generator(Typing, _, G, R) :-
    G =.. [variable | Args],
    last(Args, Var),
    append(Indexes, [Var], Args),
    \+ is_bound(Var, Typing),
    \+ are_bound(Indexes, Typing),
    R =.. [variable | Args].

generator(Typing, _, G, R) :-
    G =.. [range_element | Args],
    \+ are_bound(Args, Typing),
    R =.. [range_element | Args].

generator(_, State, violated(Name, Arg1, Arg2), ndl_cstore_violated(CStore, Name, constraint(Name, Arg1, Arg2))) :-
    ndl_state_get_cstore(State, CStore).

increment_warnings() :-
    increment_warnings(1).
increment_warnings(V) :-
    warnings_num(X), !,
    Y is X + V, 
    retract(warnings_num(_)),
    assert(warnings_num(Y)).


are_bound([], _).
are_bound([H|T], C) :-
    is_bound(H, C),
    are_bound(T, C).
is_bound(Var, C) :- 
    ndl_typing_of(Var, C, _).

are_different(X) :- 
     term_variables(X, Z),
     length(X, LX),
     length(Z, LZ),
     LX = LZ.

:- begin_tests(noodle_query_unit).

test(compile_basic_term) :-
    compile_term(!, State, !, State).
test(compile_get_value) :-
    compile_term(get_value(var, Val), State, ndl_get_value(State, var, Val), State).
test(compile_set_value) :-
    compile_term(set_value(var, val), State, ndl_set_value(State, var, val, NewState), NewState).
test(compile_swap_values) :-
    compile_term(swap_values(var1, var2), State, ndl_swap_values(State, var1, var2, NewState), NewState).

test(compile_basic_single_term_query) :-
    compile_query([!], State, [!], State).
test(compile_single_term_query) :-
    compile_query([set_value(var, val)], State, [ndl_set_value(State, var, val, NewState)], NewState).
test(compile_multiple_term_query) :-
    Query = [set_value(var1, val1), swap_values(var2, var3)],
    Expected = [ndl_set_value(Start, var1, val1, Second), ndl_swap_values(Second, var2, var3, Finish)],
    compile_query(
        Query,
        Start,   
        Result,
        Finish
    ),
    assertion(Result = Expected). 

:- end_tests(noodle_query_unit).