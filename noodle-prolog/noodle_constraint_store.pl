:- module(noodle_constraint_store, [
    ndl_cstore_empty/1,
    ndl_cstore_new/3,
    ndl_cstore_is/1,
    ndl_cstore_get/3,
    ndl_cstore_gen/3,
    ndl_cstore_update_sat/3,
    ndl_cstore_update_unsat/3,
    ndl_cstore_violated/3,
    ndl_cstore_satisfied/3,
    ndl_cstore_keys/2
]).

:- use_module(library(assoc)).

ndl_cstore_empty(cstore{sat: _, unsat: _}).

ndl_cstore_new(N, State, cstore{sat:SatisfiedSet, unsat:ViolatedSet}) :-
    findall(C-A, (N:available_constraint(C), satisfied_assoc(N, State, C, A)), SatData),
    dict_pairs(SatisfiedSet, sat, SatData),
    findall(C-A, (N:available_constraint(C), unsatisfied_assoc(N, SatisfiedSet.C, C, A)), UnsatData),
    dict_pairs(ViolatedSet, unsat, UnsatData).

satisfied_assoc(N, State, C, Assoc) :-
    findall(constraint(C, A1, A2) - 1, 
        (N:constraint(C, A1, A2),
        N:is_satisfied(C, State, A1, A2)),
        SatPairs
    ),
    list_to_assoc(SatPairs, Assoc).
unsatisfied_assoc(N, SatAssoc, C, UnsatAssoc) :-
    findall(constraint(C,A1,A2) - 0, 
        (N:constraint(C, A1, A2),
         \+ get_assoc(constraint(C, A1, A2), SatAssoc, 1)),
        UnsatPairs
    ),
    list_to_assoc(UnsatPairs, UnsatAssoc).

ndl_cstore_is(cstore{sat:_, unsat:_}).

ndl_cstore_get(CStore, Constraint, Value) :-
    Constraint = constraint(Name, _, _),
    get_assoc(Constraint, CStore.sat.Name, Value).
ndl_cstore_get(CStore, Constraint, Value) :-
    Constraint = constraint(Name, _, _),
    get_assoc(Constraint, CStore.unsat.Name, Value).
ndl_cstore_gen(CStore, Constraint, Value) :-
    Constraint = constraint(Name, _, _),
    gen_assoc(Constraint, CStore.sat.Name, Value).
ndl_cstore_gen(CStore, Constraint, Value) :-
    Constraint = constraint(Name, _, _),
    gen_assoc(Constraint, CStore.unsat.Name, Value).

ndl_cstore_violated(CStore, Name, Violated) :-
    gen_assoc(Violated, CStore.unsat.Name, _).

ndl_cstore_satisfied(CStore, Name, Satisfied) :-
    gen_assoc(Satisfied, CStore.sat.Name, _).

ndl_cstore_keys(CStore, Keys) :-
    dict_keys(CStore.unsat, Keys).

ndl_cstore_update_unsat(CStore, Constraint, cstore{sat:NSat, unsat:NUnsat}) :-
    Constraint = constraint(Name, _, _),
    del_assoc(Constraint, CStore.sat.Name, 1, NameSat), !,
    put_dict(Name, CStore.sat, NameSat, NSat),
    put_assoc(Constraint, CStore.unsat.Name, 0, NameUnsat), 
    put_dict(Name, CStore.unsat, NameUnsat, NUnsat). 
ndl_cstore_update_unsat(CStore, _, CStore).
ndl_cstore_update_sat(CStore, Constraint, cstore{sat:NSat, unsat:NUnsat}) :-
    Constraint = constraint(Name, _, _),
    del_assoc(Constraint, CStore.unsat.Name, 0, NameUnsat), !,
    put_dict(Name, CStore.unsat, NameUnsat, NUnsat),
    put_assoc(Constraint, CStore.sat.Name, 1, NameSat),
    put_dict(Name, CStore.sat, NameSat, NSat).
ndl_cstore_update_sat(CStore, _, CStore). 
    